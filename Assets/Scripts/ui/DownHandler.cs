﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class DownHandler : MonoBehaviour,IPointerDownHandler{

    public bool isDown = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
    }
    
}
