﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// canvas group sprite버전 내가 만들어서쓴다
/// 해당 component가 있는 object의 child전체의 sprite에 같은 효과 적용
/// 편의를 위해서 효과 = DoTween으로 한정해둠
/// </summary>
public class SpriteGroup : MonoBehaviour {

    SpriteRenderer[] sr;

    public delegate void SRAction(SpriteRenderer sr);
    // Use this for initialization
    void Awake () {
        sr = GetComponentsInChildren<SpriteRenderer>();
	}

    /// <summary>
    /// 람다식으로 sprite render를 parameter로 받는 lamda식 건내줌 => 적용
    /// </summary>
    /// <param name="tween"></param>
    public void SetAction(SRAction act)
    {
        for (int i = 0; i < sr.Length; i++)
            act(sr[i]);
    }
    
	
}
