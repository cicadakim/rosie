﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToStageBtnScr : MonoBehaviour {

    //public 변수로 설정해주고 GotoStage->SCM.ingame에 정보전달

    public Sprite bg;
    public Sprite planet;
    public GameObject bossPref;
    public Sprite bhp_gauge;
    public Sprite bhp_ruler;
    
    public void SetLevel(int level)
    {
        SceneManagerScr SCM = SceneManagerScr.instance;
        if (SCM == null) return;
        SCM.stageInfo.level = level;
    }

    public void GoToStage(bool isTutorial=false)
    {
        SceneManagerScr SCM = SceneManagerScr.instance;
        if (SCM == null) return;
        SCM.SetStageInfo(bg,planet,bossPref,bhp_gauge,bhp_ruler);
        if (!isTutorial)
            SCM.GoToIntro();
        else
            SCM.GoToTutorial();
    }

}
