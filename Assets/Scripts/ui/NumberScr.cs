﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class NumberScr : MonoBehaviour
{
    NumberManagerScr NM;
    List<DigitScr> digitList;
    public Sequence tfTween;
    public Sequence cgTween;

    Sprite[] NUM_SET; // 해당 Number에 쓰일 0~9까지 Sprite Set [0]에 0이들어감
    float width, height; // 전체 Number wh
    int number;
    int length;

    public delegate Sequence TweenCG(CanvasGroup cg); 
    public delegate Sequence TweenTF(Transform t);

    CanvasGroup cg;

    private void Awake()
    {
        NM = NumberManagerScr.Instance;
        digitList = new List<DigitScr>(); // digit 기록 => [0]이 제일 뒷자리
        cg = GetComponent<CanvasGroup>();
    }

    /// <summary>
    /// pool에서 꺼내진 후 하는 모든 초기화작업
    /// </summary>
    public void InitNum(int num, Sprite[] spriteset,Transform parent=null)
    {
        if (parent != null)
            transform.SetParent(parent);
        NUM_SET = spriteset;
        SetNum(num);
    }
    /// <summary>
    /// number 부여   
    /// digit을 pool(NM)에서 꺼내온후 자식으로 등록 
    /// isInit이 false일때는 이미 length가 갖춰져있다고 생각함
    /// </summary>
    private void SetNum(int num, bool isInit=true)
    {
        number = num;
        length = NM.NumDigit(num);
        
        float oneWidth = NUM_SET[0].rect.width;
        width = oneWidth * length;
        height = NUM_SET[0].rect.height;
        

        float x = width / 2 - oneWidth / 2; // wdith크기의 number에서 local의 끝점 (가운데점)

        for (int i = 0; i < length; i++)
        {
            if(isInit)
                digitList.Add(NM.GetEmptyDigit()); // ChangeNum의경우 add할필요없음

            int d = NM.FindDigit(i, num); // 뒤자리부터 넣는중

            digitList[i].SetSprite(NUM_SET[d]);
            digitList[i].transform.SetParent(transform);
            digitList[i].transform.localScale = Vector3.one;
            digitList[i].transform.rotation = Quaternion.Euler(0,0,transform.rotation.eulerAngles.z); // pool에서 꺼내질때 Number object각도 정해짐
            digitList[i].transform.localPosition = new Vector3(x,0,0); // rotation을 돌리고 local로 이동시키면 해당 축을 따라감.
            
            x -= oneWidth; // 다음자릿수를 위해 이동
        }
        
    }

    public void ChangeNum(int num)
    {
        int numDigit = NM.NumDigit(num);
        if (numDigit == length)
        {
            for (int i = 0; i < numDigit; i++)
                digitList[i].SetSprite(NUM_SET[NM.FindDigit(i, num)]);
            return;
        }
        else if (numDigit > length)
            for (int i = 0; i < numDigit - length; i++)
            {
                digitList.Add(NM.GetEmptyDigit());
            }

        else
            for (int i=length-numDigit-1; i>=0; i--)
                RemoveDigit(i);
        
        SetNum(num, false);
    }

    /// <summary>
    /// CanvasGroup 관련된 Tween 등록
    /// </summary>
    public void SetTweenCG(TweenCG with, bool isEnd = false)
    {
        if (cgTween != null && cgTween.IsPlaying())
            cgTween.Kill();

        cgTween = with(cg);
        if (isEnd)
            cgTween.OnComplete( ()=> { ReleaseNumber(); } );
    }

    /// <summary>
    /// Transform 관련된 Tween 등록 => number Object에 등록
    /// Tween등록시 이전 Tween은 제거됨(1Tween for 1number..)
    /// </summary>
    public void SetTweenTF(TweenTF with, bool isEnd=false)
    {
        if (tfTween != null && tfTween.IsPlaying())
            tfTween.Kill();
        tfTween = with(transform);

        if (isEnd)
            tfTween.OnComplete( ()=> { ReleaseNumber(); } );
    }

    /// <summary>
    /// 밑에서 i번째(0부터시작)의 Tween을 가져옴
    /// </summary>
    public Sequence GetDigitTween(int idx)
    {
        if (idx >= 0 && idx < digitList.Count)
            return digitList[idx].digitTween;
        else
            return null;
    }


    /// <summary>
    /// list remove :: iterating시 조심
    /// </summary>
    void RemoveDigit(int i)
    {
        NM.SleepDigit(digitList[i]);
        digitList.RemoveAt( i );
        length--;
    }


    public void ReleaseNumber()
    {
        tfTween.Kill();
        cgTween.Kill();
        for (int i = digitList.Count-1; i>=0; i--)
        {
            RemoveDigit(i);
        }
        digitList.Clear();
        NM.SleepNum(this);
        
    }

}

