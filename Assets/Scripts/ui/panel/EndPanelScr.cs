﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class EndPanelScr : Panel {

    public Image endLabel;

    public Sprite winSpr;
    public Sprite loseSpr;
    public Image effectWin;
    
    /// <summary>
    /// Panel activate
    /// </summary>
    public void PanelOn(bool isWin)
    {
        gameObject.SetActive(true);

        Sequence endEffect = DOTween.Sequence();
        endEffect.Append(endLabel.transform.DOScale(1.2f, 0.2f));
        endEffect.Append(endLabel.transform.DOScale(1.0f, 0.5f));

        if (isWin)
        {
            //effectWin.gameObject.SetActive(true);
            //DOTweenCustom.Blink(effectWin,-1,0.4f,0.7f,false); 
            endLabel.sprite = winSpr;
        }
        else
            endLabel.sprite = loseSpr;
    }
}
