﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Panel : MonoBehaviour
{
    public Transform background;
    
    // Use this for initialization
    void Start()
    {
        background.localScale = new Vector3(SceneManagerScr.instance.bgScaleX, 1, 1); // 배경만 화면에 맞게 늘리거나 줄여줌   
    }


}