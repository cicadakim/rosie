﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanelScr : Panel {
    	
    public void Pause()
    {
        if (gameObject.activeSelf)
            return;
        gameObject.SetActive(true);
    }
}
