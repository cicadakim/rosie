﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanelScr : Panel {

    SceneManagerScr SCM;

    public Image sound_switch;
    public Image vibe_switch;

    public Sprite switch_on;
    public Sprite switch_off;

    private void Start()
    {
        SCM = SceneManagerScr.instance;
        UpdateSoundSwitch();
        UpdateVibeSwitch();
    }

    void UpdateSoundSwitch()
    {
        if (SCM.isSoundOn)
        {
            sound_switch.sprite = switch_on;
        }
        else
            sound_switch.sprite = switch_off;
    }
    void UpdateVibeSwitch()
    {
        if (SCM.isVibeOn)
        {
            vibe_switch.sprite = switch_on;
        }
        else
            vibe_switch.sprite = switch_off;
    }

    public void SoundOnOFF()
    {
        SCM.isSoundOn = !SCM.isSoundOn;

        int soundOn = SCM.isSoundOn ? 1 : 0;
        PlayerPrefs.SetInt("soundOn", soundOn);
        PlayerPrefs.Save();

        if(SCM.isSoundOn)
            SoundManager.Mute();
        else
        {
            SoundManager.Mute();
        }

        UpdateSoundSwitch();
    }

    public void VibeOnOFF()
    {
        SCM.isVibeOn = !SCM.isVibeOn;
        int vibeOn = SCM.isVibeOn ? 1 : 0;
        PlayerPrefs.SetInt("vibeOn", vibeOn);
        PlayerPrefs.Save();
        UpdateVibeSwitch();
    }


}
