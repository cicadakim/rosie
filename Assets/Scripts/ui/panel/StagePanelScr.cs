﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StagePanelScr : Panel {

    public ToStageBtnScr targetStage;


    public void SetTargetStage(ToStageBtnScr scr)
    {
        targetStage = scr;
    }

    public void GoStage(int level)
    {
        targetStage.SetLevel(level);
        targetStage.GoToStage();
    }

}
