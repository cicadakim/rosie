﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
/// <summary>
/// Number의 각 숫자를 나타내는 Object
/// </summary>
public class DigitScr : MonoBehaviour
{
    public Sequence digitTween;
    Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    public void SetSprite(Sprite spr)
    {
        image.sprite = spr;
        image.SetNativeSize();
    }
}

