﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


/// <summary>
/// 터치받아서 전달. 버튼 눌려진거면 버튼이 알아서 하도록
/// </summary>
public class TouchManagerScr : MonoBehaviour
{
    public UnityEvent onRemain = null; // 아무 버튼도 안눌렸을때
    public DownHandler[] buttonArr;
    
    void Awake()
    {
    }

    void Update()
    {
        CheckInput();
    }

    void CheckInput()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
        {
            if (Input.GetKeyDown(KeyCode.Space))
                occurRemain();

            if (Input.GetMouseButtonDown(0))
            {
                int i = 0;
                for (; i < buttonArr.Length; i++)
                {
                    DownHandler sb = buttonArr[i];
                    if (sb.isDown)
                        break;
                }
                if (i == buttonArr.Length)
                    occurRemain();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                for (int i=0; i < buttonArr.Length; i++)
                {
                    DownHandler sb = buttonArr[i];
                    sb.isDown = false;
                }
            }
        }
        if (Input.touchCount <= 0) return;

        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Began)
            {
                int j = 0;
                for (; j< buttonArr.Length; j++)
                {
                    DownHandler sb = buttonArr[j];
                    if (sb.isDown)
                        break;
                }
                if (j == buttonArr.Length)
                    occurRemain();
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                for (int k = 0; k < buttonArr.Length; k++)
                {
                    DownHandler sb = buttonArr[k];
                    sb.isDown = false;
                }
            }
        }
    }
    
    void occurRemain()
    {
        if (onRemain != null) onRemain.Invoke();
    }

}

