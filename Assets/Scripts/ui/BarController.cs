﻿using System.Collections.Generic;
using UnityEngine;
using MEC;
using DG.Tweening;
public class BarController
{
    public Transform hpBarMask;
    public Transform hpBarGauge;
    
    SpriteRenderer gaugeSR;
    
    float width,height;
    bool isHorizontal;
    public BarController(Transform scaler, Transform gauge, bool isHorizontal = true)
    {
        hpBarMask = scaler;
        hpBarGauge = gauge;
        
        gaugeSR = hpBarGauge.GetComponent<SpriteRenderer>();
        
        width = gaugeSR.sprite.bounds.size.x;
        height = gaugeSR.sprite.bounds.size.y;
        this.isHorizontal = isHorizontal;
    }

    /// <summary>
    /// Hp Bar를 Max hp대비 hp에 걸맞게 표시해줌
    /// 
    /// </summary>
    public void UpdateCurrentHP(float hp, float max_hp)
    {
        if (isHorizontal)
            UpdateHorizontalHP(hp,max_hp);
        else
            UpdateVerticalHP(hp,max_hp);
    }

    void UpdateHorizontalHP(float hp, float max_hp)
    {
        float per = (1.0f - (hp / max_hp)) * width;
        hpBarMask.localPosition = -new Vector3(per, 0, 0);
        hpBarGauge.localPosition = new Vector3(per, 0, 0);
    }

    void UpdateVerticalHP(float hp, float max_hp)
    {
        float per = (1.0f - (hp / max_hp)) * height;
        hpBarMask.localPosition = -new Vector3(0, per, 0);
        hpBarGauge.localPosition = new Vector3(0, per, 0);
    }

    /// <summary>
    /// hp가 0으로 보이도록 표시
    /// </summary>
    public void ResetBar()
    {
        hpBarGauge.localPosition = new Vector3(-width,0,0);   
    }

    /// <summary>
    /// Bar를 초기상태에서 원상복귀시키는 애니메이션
    /// </summary>
    public IEnumerator<float> RefillBar(float duration)
    {
        ResetBar();
        yield return Timing.WaitUntilDone(DOTweenCustom.WaitForTween(hpBarGauge.DOLocalMoveX(0, duration)));
    }



}

