﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using DG.Tweening;

public class SetelliteScr : Enemy
{
    public float frontTheta;
    bool isDead;

    LaserWeapon laserW;
    public GameObject laserObj;
    Timer laserTimer;
    public Sprite laserWaitSpr, laserActiveSpr;
    public Sprite normalSpr, targetSpr;
    SpriteRenderer spriteRenderer;

    PlayerScr target;

    bool isShooting;
    /// <summary>
    /// 오브젝트풀에서 꺼낸다음에 반드시 초기화 해줘야함
    /// Setellite마다 패턴을 넣어서 관리한다(조립패턴)
    /// </summary>
    public void InitSetellite (float front, Vector2 pos ,int maxhp,Transform laserParent)
    {
        isShooting = false;
        frontTheta = front;
        MAX_HP = hp = maxhp;
        isDead = false;
        laserW = new LaserWeapon(laserObj,laserParent,pos);
        laserTimer = new Timer(1.2f);
        spriteRenderer = GetComponent<SpriteRenderer>();
        target = ObjectManagerScr.Instance.GetPsc();
        spriteRenderer.sprite = normalSpr;

        Timing.RunCoroutine(StartMove(pos,0.8f));
    }

    IEnumerator<float> StartMove(Vector2 pos, float time)
    {
        Tweener moveTween = transform.DOMove(pos, time);
        while(moveTween.IsPlaying())
        {
            if (!gameObject.activeSelf)
            {
                moveTween.Kill();
                break;
            }
            yield return Timing.WaitForOneFrame;
        }
        Timing.RunCoroutine(ScrUpdate());
        yield return Timing.WaitForOneFrame;
    }
    
    IEnumerator<float> ScrUpdate()
    {
        while (!isDead)
        {
            if (GameManagerScr.Instance.eventOccur) // quest :: event listener달아주자 제발!
                yield return 0f;
            if (!gameObject.activeSelf)
                break;

            // quest :: boss4의 레이저랑 합쳐서 특정구역에 들어오면 레이저 쏘는 패턴으로 아예 패턴클래스를 만들수있을듯
            bool isFrontPlayer = MathComponent.IsInRangeFromOrigin(target.currentTheta, frontTheta, 90);

            if (isFrontPlayer) // 범위안에 들어옴
            {
                if (!isShooting) // 처음들어옴
                {
                    spriteRenderer.sprite = targetSpr;
                    isShooting = true;
                    laserTimer.resetTimer();
                }
                if (laserTimer.IsStop())
                {
                    //레이저발사
                    
                    laserW.Shot(target.currentTheta + UnityEngine.Random.Range(-5, 6), 10, 0.8f, 0.5f, laserWaitSpr, laserActiveSpr);
                    laserTimer.resetTimer();
                }
                laserTimer.Update(Time.deltaTime);
            }
            else
            {
                if (isShooting)
                {
                    spriteRenderer.sprite = normalSpr;
                    isShooting = false; // 빠져나감
                    laserTimer.StopTimer();
                }
            }
            yield return Timing.WaitForOneFrame;
        }
    }


    public override void EnterAttack(float damage,Collider2D other)
    {
        //ObjectManagerScr.Instance.GetPsc().shooter.RemoveBullet(other.gameObject);
        if (Damaged(damage))
            isDead = true;
        
    }

    public void Dead()
    {
        isDead = true;
    }
    
    public bool IsDead()
    {
        return isDead;
    }
}

