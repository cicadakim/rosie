﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;
using MEC;

public class Boss3Scr : Boss
{
    public SpriteRenderer faceRend;
    public Sprite faceNorm;
    public Sprite faceUpset;
    public Sprite faceHit;

    public GameObject leftArm;
    public GameObject rightArm;

    public GameObject leftFirePref, rightFirePref;
    GameObject leftFire, rightFire;

    public GameObject armAnim; // 대기중일때 발사 추진 애니메이션

    public GameObject attackTargetPref;
    GameObject attackTarget;

    public GameObject easeBullet;
    EaseSetter easeSetter;
    public Transform firePlace;
    public AnimationCurve easeCurve;
    
    Animator animator;
    Sequence attackTween;

    bool isLockon = false;

    override protected void AwakeBossProperty()
    {
        hintStr = "Don't face with him";
    }

    override protected void InitBossProperty()
    {
        currentTheta = 0;
        
        animator = GetComponent<Animator>();

        animator.speed = 0;

        attackTarget = Instantiate(attackTargetPref, new Vector3(0, 0,0), Quaternion.identity, Camera.main.transform);
        leftFire = Instantiate(leftFirePref,Vector3.zero,Quaternion.identity , transform);
        rightFire = Instantiate(rightFirePref, Vector3.zero, Quaternion.identity, transform);//DoTween땜에 결국 보스에 귀속시키기로..

        InitShooting();
    }
    
    void InitShooting()
    {
        shooter.InitBulletType(BulletScr.TYPE_NORM, normBullet, 10);
        shooter.InitBulletType(BulletScr.TYPE_EASE, easeBullet, 10);

        easeSetter = new EaseSetter();
        easeSetter.InitSetter(easeCurve);
    }

    protected override void OnGrowEnd()
    {
        animator.speed = 1;
    }

    public override void OnGameStart()
    {
        base.OnGameStart();
        StartPattern(1, DoPattern1());
    }

    /// <summary>
    /// 팔 대기끝->발사
    /// </summary>
    public void ArmFire(GameObject arm, GameObject fire,Vector3 to)
    {
        armAnim.SetActive(false);

        fire.transform.position = arm.transform.position;
        fire.transform.rotation = arm.transform.rotation;

        fire.gameObject.SetActive(true);
        arm.gameObject.SetActive(false);
        
        attackTween = DOTween.Sequence();
        attackTween.Append(fire.transform.DORotate(new Vector3(0, 0, SM.WhereTheta(attackTarget.transform.position.x, attackTarget.transform.position.y)), 0.1f));
        attackTween.Join(fire.transform.DOMove(to, 0.3f).SetEase(Ease.OutBack));
    }

    /// <summary>
    /// 발사했던 팔 돌아옴
    /// </summary>
    public void ArmBack(GameObject arm, GameObject fire)
    {
        fire.transform.Translate(Vector3.zero);
        fire.gameObject.SetActive(false);
        arm.SetActive(true);
    }

    protected virtual IEnumerator<float> DoPattern1()
    {
        isPatternRunning = true;
        float attackCool = 0;
        const float attackCoolTime = 0.5f;

        Shooting(ShootingCrossRotate, 1.8f);

        while (isPatternRunning)
        {
            if (isNextBound)
            {
                isNextBound = false;
                isPatternRunning = false;
                break;
            }
            attackCool += Time.deltaTime;
            Rotate(30, 1);
            if (attackCool > attackCoolTime && FindPlayerInRange(15))
            {
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(LeftAttack(3.0f)));
                attackCool = 0;

            }
            yield return Timing.WaitForOneFrame;
        }
        StartPattern(2,DoPattern2());
    }

    IEnumerator<float> DoPattern2()
    {
        isPatternRunning = true;
        float attackCool = 0;
        const float attackCoolTime = 0.5f;

        ClearShootingPattern();
        Shooting(ShootingCrossRotateWithEase, 1.8f);

        while (isPatternRunning)
        {
            if (isNextBound)
            {
                isNextBound = false;
                isPatternRunning = false;
                break;
            }
            attackCool += Time.deltaTime;
            Rotate(50, 1);
            if (attackCool > attackCoolTime && FindPlayerInRange(15))
            {
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(LeftAttack(3.0f)));
                attackCool = 0;
            }
            yield return Timing.WaitForOneFrame;
        }

        StartPattern(3,DoPattern3());
    }

    IEnumerator<float> DoPattern3()
    {
        isPatternRunning = true;

        float attackCool = 0;
        const float attackCoolTime = 0.5f;

        while(isPatternRunning)
        {
            if (isNextBound)
                break;
            
            attackCool += Time.deltaTime;
            Rotate(50, 1);
            if (attackCool > attackCoolTime && FindPlayerInRange(15))
            {

                CoroutineHandle shootingFront = Shooting(ShootingInFront, 1.0f);
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(LeftAttack(3.0f)));
                Timing.KillCoroutines(shootingFront);
                attackCool = 0;
            }

            yield return Timing.WaitForOneFrame;
        }
        isPatternRunning = false;
    }


    IEnumerator<float> AttackBack()
    {
        Tween so = DOTweenCustom.ShakeObject(leftFire.transform, 0.4f, 6, 0.5f);
        yield return Timing.WaitUntilDone(DOTweenCustom.WaitForTween( so ));  
        
        faceRend.sprite = faceNorm;
        ArmBack(leftArm,leftFire);
        animator.enabled = true;
        animator.SetTrigger("Idle");
        yield return Timing.WaitForOneFrame;
    }

    void AttackLockOn(int dir,float dist)
    {
        isLockon = true;
        float playerTheta = ObjectManagerScr.Instance.GetPsc().currentTheta;
        float playerSpeed = ObjectManagerScr.Instance.GetPsc().currentSpeed;
        attackTarget.transform.position = SM.WhereInDegree(playerTheta + playerSpeed * dist * dir);
        attackTarget.transform.rotation = Quaternion.identity;
        attackTarget.transform.Rotate(new Vector3(0,0, SM.WhereTheta(attackTarget.transform.position.x, attackTarget.transform.position.y)));
    }

    void Rotate(float theta,int dir)
    {
        float addTheta = Time.deltaTime * theta * dir;
        transform.Rotate(new Vector3(0, 0, addTheta));
        currentTheta = transform.rotation.eulerAngles.z;
    }

    void FocusToPlayer()
    {
        float playerTheta = ObjectManagerScr.Instance.GetPsc().currentTheta;
        transform.localEulerAngles = new Vector3(0,0,playerTheta);
    }

    bool FindPlayerInRange(int halfrange)
    {
        float playerTheta = ObjectManagerScr.Instance.GetPsc().currentTheta;

        if (currentTheta <= playerTheta + halfrange && currentTheta >= playerTheta - halfrange)
        {
            return true;
        }
        return false;
    }
    
    //WaitBeforeFire :: 빨간색 얼굴이 된 후 쏘기까지 걸리는 시간
    IEnumerator<float> LeftAttack(float firetime, float waitBeforeFire=0.3f)
    {
        float attackWaitTime = 0;
        attackTarget.SetActive(true);
        armAnim.SetActive(true);
        animator.SetTrigger("LeftUp");
        float startAttackTime = firetime - waitBeforeFire;
        if (startAttackTime <= 0) startAttackTime = 0;

        const int waiting = 0, attacking = 1;
        int state = waiting;
        
        while (true)
        {
            switch (state)
            {
                case waiting:
                    if (attackWaitTime < firetime)
                    {
                        attackWaitTime += Time.deltaTime;
                        FocusToPlayer();
                        if (animator.enabled && attackWaitTime >= startAttackTime)
                        {
                            faceRend.sprite = faceUpset;
                            animator.enabled = false;
                        }
                        else if (attackWaitTime < startAttackTime)
                            AttackLockOn(ObjectManagerScr.Instance.GetPsc().direction,0.2f);
                            
                    }
                    else // fire left arm
                    {
                        state = attacking;
                        attackTarget.SetActive(false);
                        ArmFire(leftArm, leftFire, attackTarget.transform.position);
                    }
                    break;
                case attacking:
                    if (!attackTween.IsPlaying())
                    {
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(AttackBack()));
                        isLockon = false;
                        yield break;
                    }
                    break;
            }
            yield return Timing.WaitForOneFrame;
        }
    }

    /*
    IEnumerator<float> RightAndLeftAttack()
    {
        animator.SetTrigger("RightUp");
        attackTarget.SetActive(true);

        float firetime = 0;
        const float rightFireTime = 1.0f;
        const float leftFireTime = 1.5f;

        const int rightWait = 0, leftWait = 1 ,bulletfire = 2 ;

        int state = rightWait;

        while(true)
        {
            FocusToPlayer();
            switch (state)
            {
                case rightWait:
                    AttackLockOn(-1, 0.5f);
                    firetime += Time.deltaTime;
                    if(firetime>rightFireTime)
                    {
                        ArmFire(rightArm, rightFire, attackTarget.transform.position);
                        animator.SetTrigger("LeftUp");
                        state = leftWait;
                        firetime = 0;
                    }
                    break;
                case leftWait:
                    AttackLockOn(1, 0.5f);
                    firetime += Time.deltaTime;
                    if (firetime > leftFireTime)
                    {
                        ArmFire(leftArm, leftFire, attackTarget.transform.position);
                        state = bulletfire;
                        ArmBack(rightArm, rightFire);
                    }

                    break;

            }
            yield return Timing.WaitForOneFrame;
        }

        yield return Timing.WaitForOneFrame;
    }
    */
    
    void ShootingInFront()
    {
        if (level == 0)
            return;
        Timing.RunCoroutine(Shot_nWay(BulletScr.TYPE_NORM , target, 30,5,3,0.2f,10,5.5f,null,bulletSprs[1]));
    }
    
    void ShootingCrossRotate()
    {
        if(level==0)
        {
            Timing.RunCoroutine(ShotRotate(BulletScr.TYPE_NORM, -1, MathComponent.ThetaIn2PI(currentTheta - 60), 220, 20, 4.5f, 0.2f, 10, null, bulletSprs[0]));
            return;
        }
        Timing.RunCoroutine(ShotRotate(BulletScr.TYPE_NORM ,-1, MathComponent.ThetaIn2PI(currentTheta-60),220,15,4.5f,0.2f,10,null,bulletSprs[0]));
        Timing.RunCoroutine(ShotRotate(BulletScr.TYPE_NORM, 1, MathComponent.ThetaIn2PI(currentTheta + 60), 220, 20, 5.5f, 0.2f, 10, null, bulletSprs[1]));
        
    }

    void ShootingCrossRotateWithEase()
    {
        if(level==0)
        {
            Timing.RunCoroutine(ShotRotate(BulletScr.TYPE_EASE, -1, MathComponent.ThetaIn2PI(currentTheta - 60), 220, 20, 3.5f, 0.2f, 10, easeSetter, bulletSprs[0]));
            return;
        }
        Timing.RunCoroutine(ShotRotate(BulletScr.TYPE_EASE, -1, MathComponent.ThetaIn2PI(currentTheta - 60), 220, 15, 3.5f, 0.2f, 10,easeSetter, bulletSprs[0]));
        Timing.RunCoroutine(ShotRotate(BulletScr.TYPE_EASE, 1, MathComponent.ThetaIn2PI(currentTheta +60), 220, 20, 4.5f, 0.2f, 10,easeSetter, bulletSprs[1]));
        
    }
    
    IEnumerator<float> HitFace(float duration)
    {
        faceRend.sprite = faceHit;
        yield return Timing.WaitForSeconds(duration);
        faceRend.sprite = faceNorm;
    }

    protected override bool OnHit(Collider2D other)
    {
        if(!isLockon)
            Timing.RunCoroutine(HitFace(0.2f));
        return false;
    }



}

