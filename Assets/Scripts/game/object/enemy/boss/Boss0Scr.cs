﻿using UnityEngine;
using System.Collections.Generic;
using System;

using MEC;

public class Boss0Scr : Boss
{
    public SpriteRenderer faceRend;

    public Sprite normFace;
    public Sprite hitFace;
    public Sprite angryFace;

    bool isAngry = false;
    bool beforeStart = true;
    
    protected override void AwakeBossProperty()
    {
    }

    protected override void InitBossProperty()
    {
    }
    protected override void InitBulletTypes()
    {
        shooter.InitBulletType(EBulletScr.TYPE_NORM, normBullet, 20);
    }

    public override void OnGameStart()
    {
        base.OnGameStart();
    }

    private new void Update()
    {
        base.Update();
        transform.rotation = OM.GetPsc().transform.rotation;
    }

    /// <summary>
    /// 튜토리얼 끝나고 패턴 시작
    /// </summary>
    public void StartPattern()
    {
        beforeStart = false;
        StartPattern(1, DoPattern1());
    }
    
    /// <summary>
    /// 플레이어 무조건 체력 다는 이벤트를 위해서
    /// </summary>
    /// <returns></returns>
    public IEnumerator<float> CantEvade()
    {
        isAngry = true;
        ClearShootingPattern();
        faceRend.sprite = angryFace;
        yield return Timing.WaitForSeconds(3.5f);
        ShotAround(BulletScr.TYPE_NORM, 2, 5.5f, 10, null, bulletSprs[1], 0);
        faceRend.sprite = normFace;
        isAngry = false;
    }

    IEnumerator<float> DoPattern1()
    {
        ClearShootingPattern();
        Shooting(ShootToPlayer, 0.3f);
        while (true)
        {
            if(isNextBound)
            {
                isNextBound = false;

                StartPattern(2, DoPattern2());
                break;
            }

            yield return 0f;
        }

        yield return 0f;
    }

    IEnumerator<float> DoPattern2()
    {
        ClearShootingPattern();

        //Shooting(Shoot2Way, 0.2f, -1);
        Shooting(Shoot3Way, 0.4f, -1);

        while (true)
        {
            if (isNextBound)
            {
                isNextBound = false;

                StartPattern(3, DoPattern3());
                break;
            }

            yield return 0f;
        }
        yield return 0f;
    }

    IEnumerator<float> DoPattern3()
    {
        ClearShootingPattern();
        Shooting(ShootAnother, 0.5f, -1);
        Shooting(ShootToPlayer, 0.3f, -1);

        yield return 0f;
    }

    void ShootToPlayer()
    {
        LinearShot(BulletScr.TYPE_NORM, OM.GetPsc().currentTheta, 5.5f, 10, null, bulletSprs[1]);
    }

    void Shoot2Way()
    {
        Timing.RunCoroutine(Shot_nWay(BulletScr.TYPE_NORM, target, 45, 2, 1, 1, 10, 4.5f, null, bulletSprs[0]));
    }

    void Shoot3Way()
    {
        Timing.RunCoroutine(Shot_nWay(BulletScr.TYPE_NORM, target, 60, 3, 1, 1, 10, 5.5f, null, bulletSprs[1]));
    }

    void ShootAnother()
    {
        Timing.RunCoroutine(Shot_nWay(BulletScr.TYPE_NORM, target, 60, 2, 3, 0.1f, 10, 5.5f, null, bulletSprs[0]));
    }

    IEnumerator<float> HitFace(float duration)
    {
        if (isAngry)
        {
            faceRend.sprite = angryFace;
            yield break;
        }
        faceRend.sprite = hitFace;
        yield return Timing.WaitForSeconds(duration);
        faceRend.sprite = normFace;
    }

    public override void EnterAttack(float damage, Collider2D other = null)
    {
        if (OnHit(other)) return;
        if (beforeStart) CreateDmgText((int)damage);
        else Damaged(damage);
    }

    protected override bool OnHit(Collider2D other)
    {
        Timing.RunCoroutine(HitFace(0.2f));
        return false;
    }
}

