﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using System;

public abstract class Boss : Enemy
{
    protected float currentTheta;

    [HideInInspector]
    public GameObject target;

    BarController hpController;

    SpriteRenderer hpRenderer;
    protected int pattern;

    bool isWorking;

    protected bool isPause;
    protected Timer pauseTimer;
    public Sprite[] bulletSprs;

    protected SpriteRenderer spriteRenderer;

    // 각 보스 패턴별 hint Text
    public string hintStr;

    // 시간 초기화 hp
    float nextBoundHp;
    float boundRatio;
    protected bool isNextBound;

    public Shooter shooter;

    public GameObject normBullet;

    protected Transform bulletGenPoint;
    protected Transform dmgTextPos;


    protected List<CoroutineHandle> shootingPattern; // 메인이 되는 슈팅패턴 코루틴 목록들
    protected bool isShootingRunning = false;
    
    protected CoroutineHandle currentPattern;
    protected bool isPatternRunning = false;

    protected delegate void ShootingPattern();
    protected Animator bossGrowEndEffect;
    
    protected SpriteGroup sg;

    protected int level;

    new void Awake()
    {
        base.Awake();
        if (gameObject.activeSelf) OM.SetBoss(this);
        
        AwakeBoss();
        isWorking = false;
    }

    /// <summary>
    /// Instance 생성이 필요한것, Start전에 로딩단에서 해줘야할것들
    /// </summary>
    void AwakeBoss()
    {
        target = OM.GetPsc().gameObject;
        Transform hpMask = GameObject.Find("bhp_scaler").transform;
        Transform hpGauge = GameObject.Find("bhp_gauge").transform;
        bulletGenPoint = GameObject.Find("BossBullet").transform;
        dmgTextPos = GameObject.Find("dmgTextPos").transform;
        sg = gameObject.AddComponent<SpriteGroup>();
        
        bossGrowEndEffect = GameObject.Find("effect_bossgrowend").GetComponent<Animator>();
        bossGrowEndEffect.gameObject.SetActive(false);

        spriteRenderer = GetComponent<SpriteRenderer>();
        
        hpController = new BarController(hpMask,hpGauge);
        hpController.ResetBar();
        
        InitShooter();
        
        AwakeBossProperty();
        gameObject.SetActive(false); // grow이후 active해줌

        gameObject.tag = "Boss";
        gameObject.layer = LayerMask.NameToLayer("Enemy");
        spriteRenderer.sortingOrder = 6;

    }
    

    // Use this for initialization
    protected void Start()
    {
        InitBoss();
    }

    void InitBoss()
    {
        if(level==0)
            MAX_HP = MAX_HP * 0.7f;
        hp = nextBoundHp = MAX_HP;
        
        boundRatio = 0.34f;
        nextBoundHp -= (int)(nextBoundHp * boundRatio);
        
        pauseTimer = new Timer();

        isPause = false;
        isNextBound = false;

        InitBossProperty();
    }

    public void SetLevel(int lev)
    {
        level = lev;
    }

    /// <summary>
    /// Awake -> Grow Animation => 끝나고나면 보스 생성(Start함수)완료 => Working으로 작동
    /// </summary>
    public IEnumerator EventGrow()
    {
        bossGrowEndEffect.gameObject.SetActive(true);
        
        while (bossGrowEndEffect.GetCurrentAnimatorStateInfo(0).IsName("bossgrow"))
            yield return null;
        sg.SetAction((SpriteRenderer sr) => { sr.color = new Color(255, 255, 255, 0); });
        gameObject.SetActive(true);
        yield return null;
        float t = 3;
        sg.SetAction((SpriteRenderer sr) => { sr.DOFade(1, t); });

        while (bossGrowEndEffect.GetCurrentAnimatorStateInfo(0).IsName("bossgrow_out"))
            yield return null;
        OnGrowEnd();
        //bossGrowEndEffect.gameObject.SetActive(false);
    }

    /// <summary>
    /// GrowEnd시 호출되는 함수
    /// </summary>
    protected virtual void OnGrowEnd(){}

    public IEnumerator EventWin()
    {
        isEnd = true;
        ClearPattern();
        yield return null;
    }

    protected virtual void OnLose() { }

    public IEnumerator EventLose()
    {
        OnLose();

        isEnd = true;
        ClearPattern();
        bossGrowEndEffect.SetTrigger("lose");
        //bossGrowEndEffect.gameObject.SetActive(true);
        sg.SetAction( (SpriteRenderer sr)=> sr.DOFade(0,1));
        
        gameObject.SetActive(false);
        yield return null;
    }

    /// <summary>
    /// 실제로 보스가 작동하기 시작. 
    /// 생성 -> Awake와 Start에서 초기화만  
    /// 그후에 보스를 working시키려면 이 함수를 호출해야함
    /// </summary>
    public virtual void OnGameStart()
    {
        isWorking = true; // 쓸일이 있을까?
    }

    public void HPReset(float duration)
    {
        Timing.RunCoroutine(hpController.RefillBar(duration));
    }

    /// <summary>
    /// LinearMove + no wps + damage shooter
    /// </summary>
    void InitShooter()
    {
        shootingPattern = new List<CoroutineHandle>();
        shooter = new Shooter(bulletGenPoint);
        InitBulletTypes();
    }


    protected virtual void AwakeBossProperty() { } // 각 보스별 Awake
    protected virtual void InitBossProperty() { } // 각 보스별 init
    /// <summary>
    /// 사용될 탄환들을 모두 초기화해줌
    /// </summary>
    protected virtual void InitBulletTypes() { }



    protected void StartPattern(int pat, IEnumerator<float> patternFunc)
    {
        if (currentPattern.IsValid && isPatternRunning)
            Timing.KillCoroutines(currentPattern);
        pattern = pat;
        currentPattern = Timing.RunCoroutine(patternFunc);
    }

    /// <summary>
    /// target 목표지점 기준으로 angle만큼의 간격을 벌려 n개의 총알을 
    /// interval의 간격으로 length개 흩뿌린다. 
    /// </summary>
    protected IEnumerator<float> Shot_nWay(int type,GameObject target, float angle, int n, int length, float interval,float damage, float speed, BulletSetter bst,Sprite bulletSpr)
    {
        float targetTheta = target.transform.eulerAngles.z;
        float tTheta = targetTheta - angle * (n / 2);
        if (n % 2 == 0)
            tTheta += angle / 2;
        for (int i = 0; i < n; i++)
        {
            LinearShot(type, tTheta, speed, damage, bst,bulletSpr);
            tTheta += angle;
        }
        yield return Timing.WaitForSeconds(interval);
        if (length > 1) 
            Timing.RunCoroutine(Shot_nWay(type,target, angle, n, --length, interval,damage, speed,bst,bulletSpr));
        yield break;
    }
    /*
    protected IEnumerator<float> ShotRotation(int type,float degree, float speed, float damage, BulletSetter bst, int start=0)
    {

    }*/
    
        /// <summary>
        /// start부터 degree간격으로 360도 총알 발사
        /// </summary>
    protected void ShotAround(int type,float degree,float speed,float damage,BulletSetter bst,Sprite bulletSpr, int start = 0)
    {
        for (int d = start; d <= 360 + start; d += (int)degree)
            LinearShot(type, d, speed, damage, bst, bulletSpr);
    }

    protected void LinearShot(int type, float theta, float speed, float damage, BulletSetter bst, Sprite bulletSpr)
    {
        shooter.Shot(type,theta, Vector2.zero, SM.WhereInDegree(theta,-1), speed, damage, bst , bulletSpr);
    }


    protected IEnumerator<float> ShotRotate(int type, int dir, float start, float angle, float anglespeed, float speed, float firerate, int damage, BulletSetter bst = null,Sprite bulletSpr = null)
    {
        float theta = start;
        int n = (int)(angle / anglespeed);

        while (n-- > 0)
        {
            LinearShot(type, theta, speed, damage, bst,bulletSpr);
            theta += dir * anglespeed;
            theta = MathComponent.ThetaIn2PI(theta);
            yield return Timing.WaitForSeconds(firerate);
        }


        yield return Timing.WaitForOneFrame;
    }

    protected void ClearPattern()
    {
        ClearShootingPattern();
        Timing.KillCoroutines(currentPattern);
    }

    protected void ClearShootingPattern()
    {
        for (int i = 0; i < shootingPattern.Count; i++)
            Timing.KillCoroutines(shootingPattern[i]);
        Timing.PauseCoroutines("shooting");
        shootingPattern.Clear();
    }

    public float GetHp()
    {
        return hp;
    }

    
    protected void CreateDmgText(int dmg)
    {
        NumberScr dn = NumberManagerScr.Instance.CreateNumber(dmg, dmgTextPos.position.x, dmgTextPos.position.y,0,null,dmgTextPos);
        const float textTime=0.7f;
        dn.SetTweenCG((CanvasGroup cg)=> {
            return DOTween.Sequence().
                Append(cg.DOFade(0, textTime)).
                OnKill(() => cg.DOFade(1, 0));
        });
        dn.SetTweenTF((Transform t) => {
            Sequence s = DOTween.Sequence().Append(t.DOLocalMoveY(120, textTime).SetRelative()); // canvas좌표라 120
            return s;
        } ,true);
    }
    
    /// <summary>
    /// Coroutine Handle 반환
    /// 정해진 cooltime마다 건내진 shoot func 발사 (n번) => shootingPattern에 저장
    /// ShootingPattern은 
    /// n<0일경우 무한번
    /// /// </summary>
    protected CoroutineHandle Shooting(ShootingPattern shoot, float cooltime, int n = -1)
    {
        CoroutineHandle shooting = Timing.RunCoroutine(ShootingCor(shoot, cooltime, n));
        shootingPattern.Add(shooting);

        return shooting;
    }

    /// <summary>
    /// Shooting 실질적으로 수행
    /// </summary>
    protected IEnumerator<float> ShootingCor(ShootingPattern shoot, float cooltime, int n = -1)
    {
        bool endless = false;
        if (n < 0)
            endless = true;

        isShootingRunning = true;
        while (endless || n-- > 0)
        {
            yield return Timing.WaitForSeconds(cooltime);
            shoot();
        }
    }
    // Update is called once per frame
    protected void Update()
    {
        if (Time.timeScale == 0 || GM.eventOccur) return;
        if (isPause) // 보스 자체pause
        {
            if (pauseTimer.IsStop())
            {
                isPause = false;
                DOTween.PlayAll(); //  quest :: resume역할로 이걸쓰는게 맞나?
            }
            else
            {
                pauseTimer.Update(Time.deltaTime);
                return;
            }
        }
    }

    /*
     * Hit되었을때 Boss별로 처리할부분
     * return true일경우 각 Boss단에서 Damage스텝도 처리, 아닐경우 false
    */
    virtual protected bool OnHit(Collider2D other)
    {
        return false;
    }

    void CheckHp()
    {
        if (hp < nextBoundHp) // 정해진 시간내에 일정 Boss Hp를 까면 시간 초기화
        {
            isNextBound = true;
            nextBoundHp -= (int)(MAX_HP * boundRatio);
        }
    }
    

    public override void EnterAttack(float damage, Collider2D other = null)
    {
        if (isEnd || OnHit(other)) return;
        Damaged(damage);
    }

    protected override void AfterDamaged(float dmg)
    {
        CheckHp();
        //CreateDmgText((int)dmg);
        hpController.UpdateCurrentHP(hp, MAX_HP);
        if (hp <= 0) GM.GoEnd(true);
    }

    protected float ThetaRange(float targetTheta)
    {
        if (targetTheta > 360) targetTheta = targetTheta - 360;
        else if (targetTheta < 0) targetTheta += 360;
        return targetTheta;
    }

    public void GetItemPause()
    {
        Pause(3);
        shooter.RemoveAllBullet();
    }

    /// <summary>
    /// 보스 자체 pause
    /// </summary>
    /// <param name="time"></param>
    void Pause(float time)
    {
        isPause = true;
        DOTween.Pause(gameObject);
        pauseTimer.SetTimer(time);
    }

    public override void OnEventOccur()
    {
        base.OnEventOccur();
        Timing.PauseCoroutines(currentPattern);
        for (int i = 0; i < shootingPattern.Count; i++)
            Timing.PauseCoroutines(shootingPattern[i]);
        DOTween.Pause(gameObject);
    }

    public override void OnEventResume()
    {
        base.OnEventResume();
        Timing.ResumeCoroutines(currentPattern);
        for (int i = 0; i < shootingPattern.Count; i++)
            Timing.ResumeCoroutines(shootingPattern[i]);
        DOTween.Play(gameObject);
    }

}
