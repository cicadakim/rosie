﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
class Boss4Scr : Boss
{
    BlockControllerScr blockController;

    protected override void AwakeBossProperty()
    {
        base.AwakeBossProperty();    
        blockController = GetComponent<BlockControllerScr>();
    }

    protected override void InitBulletTypes()
    {
        shooter.InitBulletType(EBulletScr.TYPE_NORM, normBullet, 20);
    }

    public override void OnGameStart()
    {
        base.OnGameStart();
        StartPattern(1, DoPattern1());
    }

    IEnumerator<float> DoPattern1()
    {
        Sequence tween = blockController.TweenAllBlocksWith((Transform t) => {
            Sequence s = DOTween.Sequence();
            s.AppendInterval(0.2f);
            s.Append(t.DOScaleX(1.0f, 2.0f));
            s.Append(t.DOScaleX(0.65f, 1.0f));
            return s;
        });
        yield return Timing.WaitForOneFrame;

        while (tween.IsPlaying())
            yield return 0f;
        StartPattern(2, DoPattern2());
    }
    
    IEnumerator<float> DoPattern2()
    {
        for(int idx = 0; idx<blockController.GetBlockLength(); idx++)
        {
            blockController.TweenBlockWith(idx, (Transform t) =>{
                 Sequence s = DOTween.Sequence();
                 s.AppendInterval(idx*0.1f);
                 s.Append(t.DOScaleX(1.0f, 0.1f).SetEase(Ease.InExpo));
                 s.Append(t.DOScaleX(0.65f, 1.0f));
                 s.AppendInterval(0.5f-idx*0.1f);
                 s.SetLoops(-1);
                 return s;
             });
        }
        yield return 0f;
    }

    Sequence BlockShot(Transform t)
    {
        Sequence s = DOTween.Sequence();
        s.Append(DOTweenCustom.ShakeObject(t, 0.2f, 4, 1));
        s.Append(t.DOScaleX(1.0f, 0.1f).SetEase(Ease.InExpo));
        s.Append(t.DOScaleX(0.65f, 1.0f));
        return s;
    }




}
