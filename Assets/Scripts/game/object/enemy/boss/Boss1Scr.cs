﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
public class Boss1Scr : Boss
{
    enum face_state { sad, upset, so };

    class Face
    {
        public Face()
        {
            hitTimer = new Timer(0.2f, true);
            
        }

        public float frontTheta;
        public face_state state;
        public GameObject faceObj;
        public SpriteRenderer renderer;
        public Timer hitTimer;
    }

    public Sprite FACE_SAD;
    public Sprite FACE_UPSET;
    public Sprite FACE_SO;
    public Sprite FACE_HIT;
    
    Face[] faces;
    public GameObject[] faceObjs;
    float changeTime;
    int frontFace;
    bool isPrevUpset;
    
    Sequence bodyTween;
    
    Timer trapTimer;

    public GameObject laserObj;
    LaserWeapon laserWeapon;

    public Sprite upsetLaserWaitSpr;
    public Sprite upsetLaserActiveSpr;

    public GameObject easeBullet;

    public GameObject shield;
    EaseSetter easeSetter;
    public AnimationCurve easeCurve;

    BlockControllerScr blockController;


    protected override void AwakeBossProperty()
    {
        hintStr = "Chase the sad face.. and Watch out the ANGRY FACE";
        blockController = GetComponent<BlockControllerScr>();
    }

    protected override void InitBossProperty()
    {
        InitFaces();
        laserWeapon = new LaserWeapon(laserObj, bulletGenPoint, transform.position, 5);
        laserWeapon.AddOption(new DamageOption(10));
        easeSetter = new EaseSetter();
        easeSetter.InitSetter(easeCurve);


    }

    protected override void InitBulletTypes()
    {
        shooter.InitBulletType(EBulletScr.TYPE_NORM, normBullet, 20);
        shooter.InitBulletType(EBulletScr.TYPE_EASE, easeBullet, 20);
    }

    void InitFaces()
    {
        frontFace = CheckFront();
        faces = new Face[3];
        for (int i = 0; i < faces.Length; i++)
        {
            faces[i] = new Face();
            faces[i].faceObj = faceObjs[i];
            faces[i].renderer = faceObjs[i].GetComponent<SpriteRenderer>();
            Timing.RunCoroutine(ChangeFaceState(i, face_state.so));
        }

    }
    
    public override void OnGameStart()
    {
        base.OnGameStart();
        StartPattern(1,DoPattern1());
    }

    void Rotate()
    {
        bodyTween = DOTween.Sequence();
        bodyTween.AppendInterval(1.0f);
        bodyTween.Append(transform.DORotate(new Vector3(0, 0, 120), 1.0f).SetRelative());
        bodyTween.AppendInterval(1.0f);
        bodyTween.Append(transform.DORotate(new Vector3(0, 0, -70), 1.0f).SetRelative());
        bodyTween.OnComplete(() => Rotate());
    }

    Sequence InitBlockPiston(Transform block)
    {
        Sequence pistonAct = DOTween.Sequence();
        pistonAct.Append(block.DOScaleX(1.0f, 0.2f).SetEase(Ease.InExpo));
        pistonAct.AppendInterval(0.25f);
        pistonAct.Append(block.DOScaleX(0.65f, 1.0f));
        pistonAct.AppendInterval(0.55f);
        pistonAct.SetLoops(-1);

        return pistonAct;
    }
    

    IEnumerator<float> DoPattern1()
    {
        Sequence tween =  blockController.TweenAllBlocksWith((Transform t) => {
            Sequence s = DOTween.Sequence();
            s.AppendInterval(0.2f);
            s.Append(t.DOScaleX(1.0f, 2.0f));
            s.Append(t.DOScaleX(0.65f, 1.0f));
            return s;
        });
        yield return Timing.WaitForOneFrame;

        while(tween.IsPlaying())
            yield return 0f;
        StartPattern(2 , DoPattern2());
    }

    IEnumerator<float> DoPattern2()
    {
        SelectSadFace();
        Timer sadChangeTimer = new Timer(10);

        blockController.TweenAllBlocksWith(InitBlockPiston);
        

        ClearShootingPattern();
        if(level==0)
            shootingPattern.Add(Timing.RunCoroutine(ShootingAround_Easy(1, 18, 0, 0.5f)));
        else if(level==1)
            shootingPattern.Add(Timing.RunCoroutine(ShootingAroundCross(1, 18, 0, 0.5f)));
        while (true)
        {
            yield return 0f;
            if (isNextBound)
            {
                isNextBound = false;
                for (int i = 0; i < 3; i++)
                    if (faces[i].state == face_state.sad)
                        Timing.RunCoroutine(ChangeFaceState(i, face_state.upset, true));
                StartPattern(3,DoPattern3());
                break;
            }

            if (sadChangeTimer.IsStop())
            {
                SelectSadFace();
                sadChangeTimer.resetTimer();
            }
            
            sadChangeTimer.Update(Time.deltaTime);
        }
    }

    IEnumerator<float> DoPattern3()
    {
        PlayerScr ps = OM.GetPsc();

        bool isLast = false;
        
        isPrevUpset = false;
        Timer upsetLaserTimer = new Timer(1.5f);
        trapTimer = new Timer(5);

        Timing.RunCoroutine(BeginRotate());
        ClearShootingPattern();

        if (level==0)
            shootingPattern.Add(Timing.RunCoroutine(ShootingAround_Easy(1, 18, 0, 0.5f)));
        else if(level==1)
            shootingPattern.Add(Timing.RunCoroutine(ShootingAroundCross(1, 18, 0, 0.7f)));

        while (true)
        {
            yield return 0f;

            if(isNextBound && !isLast)
            {
                Timing.RunCoroutine(LastBound());
                isLast = true;
                isNextBound = false;
            }
            
            if(isNextBound && isLast) // ending가기전
            {

            }

            int frontIdx = CheckFront();

            if (faces[frontIdx].state == face_state.upset) // 화난얼굴이 플레이어와 마주볼경우
            {
                if (!isPrevUpset)
                {
                    isPrevUpset = true; // 화난얼굴과 마주보는곳에 플레이어가 진입
                    upsetLaserTimer.resetTimer();
                }
                if (upsetLaserTimer.IsStop())
                {
                    //레이저발사
                    laserWeapon.Shot(ps.currentTheta, 10, 0.8f, 0.7f, upsetLaserWaitSpr, upsetLaserActiveSpr);
                    upsetLaserTimer.resetTimer();
                }
                upsetLaserTimer.Update(Time.deltaTime);
            }
            else
            {
                if (isPrevUpset)
                {
                    isPrevUpset = false; // 빠져나감
                    upsetLaserTimer.StopTimer();
                }
            }

        }
    }

    IEnumerator<float> ShootingAround_Easy(int dir, int dAngle, int start, float cooltime, int n = -1)
    {
        int aroundShotDegree = start;
        bool endless = false;
        if (n < 0)
            endless = true;

        while (endless || n-- > 0)
        {
            ShotAround(EBulletScr.TYPE_NORM, 90, 5.0f, 10, null, bulletSprs[0], aroundShotDegree);
            yield return Timing.WaitForSeconds(cooltime);
            aroundShotDegree += dAngle * dir;
            if (aroundShotDegree >= 360) aroundShotDegree = 0;
        }
    }

    /// <summary>
    /// start부터 cooltime간격으로 degreeAngle만큼 start이동해서 aroundShot발사
    /// quest :: 설계 장난??
    /// </summary>
    IEnumerator<float> ShootingAround(int dir,int dAngle,int start , float cooltime, int n=-1)
    {
        int aroundShotDegree = start;
        bool endless = false;
        if (n < 0)
            endless = true;

        while (endless || n-- > 0)
        {
            ShotAround(EBulletScr.TYPE_NORM, 48, 4.5f, 10, null, bulletSprs[0], aroundShotDegree);
            yield return Timing.WaitForSeconds(cooltime);
            aroundShotDegree += dAngle*dir;
            if (aroundShotDegree >= 360) aroundShotDegree = 0;
        }
    }

    IEnumerator<float> ShootingAroundCross_Easy(int dir, int dAngle, int start, float cooltime, int n = -1)
    {
        int aroundShotDegree = start;
        bool endless = false;
        if (n < 0)
            endless = true;

        while (endless || n-- > 0)
        {
            ShotAround(EBulletScr.TYPE_NORM, 150, 5.5f, 10, null, bulletSprs[0], aroundShotDegree);
            ShotAround(EBulletScr.TYPE_NORM, 60, 4.5f, 10, null, bulletSprs[1], 360 - aroundShotDegree);

            yield return Timing.WaitForSeconds(cooltime);
            aroundShotDegree += dAngle * dir;
            if (aroundShotDegree >= 360) aroundShotDegree = 0;
        }

    }
    
    IEnumerator<float> ShootingAroundCross(int dir, int dAngle, int start, float cooltime, int n = -1)
    {
        int aroundShotDegree = start;
        bool endless = false;
        if (n < 0)
            endless = true;

        while (endless || n-- > 0)
        {
            ShotAround(EBulletScr.TYPE_NORM, 120, 5.5f, 10, null, bulletSprs[0], aroundShotDegree);
            ShotAround(EBulletScr.TYPE_NORM, 45, 4.5f, 10, null, bulletSprs[1], 360 - aroundShotDegree);

            yield return Timing.WaitForSeconds(cooltime);
            aroundShotDegree += dAngle * dir;
            if (aroundShotDegree >= 360) aroundShotDegree = 0;
        }
        
    }
    IEnumerator<float> ShootingAroundCrossWithEase(int dir, int dAngle, int start, float cooltime, int n = -1)
    {
        int aroundShotDegree = start;
        bool endless = false;
        if (n < 0)
            endless = true;
        
        while (endless || n-- > 0)
        {
            ShotAround(BulletScr.TYPE_EASE, 120, 5.5f, 10, easeSetter, bulletSprs[0], aroundShotDegree);
            ShotAround(EBulletScr.TYPE_NORM, 45, 4.5f, 10, null, bulletSprs[1], 360 - aroundShotDegree);

            yield return Timing.WaitForSeconds(cooltime);
            aroundShotDegree += dAngle * dir;
            if (aroundShotDegree >= 360) aroundShotDegree = 0;
        }

    }

    IEnumerator<float> LastBound()
    {
        blockController.PauseAll();
        bodyTween.Pause();

        for (int i = 0; i < faces.Length; i++)
            if (faces[i].state != face_state.upset)
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(ChangeFaceState(i, face_state.upset, true)));

        blockController.ResumeAll();
        bodyTween.Play();

        ClearShootingPattern();
        if (level == 0)
            shootingPattern.Add(Timing.RunCoroutine(ShootingAround_Easy(1, 18, 0, 0.5f)));
        else if (level == 1)
            shootingPattern.Add(Timing.RunCoroutine(ShootingAroundCrossWithEase(1, 18, 0, 0.7f)));
    }

    IEnumerator<float> BeginRotate() // rotate와 piston 주기를 맞춰주기위함
    {
        Sequence block1Tween = blockController.GetBlockTween(0);
        Debug.Log(block1Tween);
        int cl = block1Tween.CompletedLoops();
        while (cl >= block1Tween.CompletedLoops())
            yield return Timing.WaitForOneFrame;
        Rotate();
        yield break;
    }
    
    /*
    //어떤 머리가 어떤 type의 공격을 어떤 주기로 하는가
    void Shooting(float dt, int face, float minR, float maxR, float bSpeed, float targetTheta, int type = 0)
    {
        Timer t = faces[face].shootTimer;

        if (!t.IsStop())
            t.Update(dt);
        else
        {
            t.SetTimer(Random.Range(minR, maxR));
            float ptheta = targetTheta;
            switch (type)
            {
                case 0:
                    aroundShotStart += aroundShotDir * 18;
                    if (aroundShotStart >= 360 || aroundShotStart <= -360) aroundShotStart = 0;
                    ShotAround(EBulletScr.TYPE_NORM, 48, 4.5f, 10, null, bulletSpr_norm, aroundShotStart);

                    break;
                case 1:// multishot
                    /*
                    int nRand = Random.Range(1, 4);
                    int isPre = Random.Range(0, 5);

                    Fire_nShot(ptheta, nRand, isPre, bSpeed);
                    //ShotAround(60);
                    break;
                case 2:
                    aroundShotStart += 36;
                    if (aroundShotStart >= 360 || aroundShotStart <= -360) aroundShotStart = 0;
                    ShotAround(EBulletScr.TYPE_NORM, 120, 5.5f, 10, null, bulletSpr_norm, aroundShotStart);
                    ShotAround(EBulletScr.TYPE_NORM, 45, 4.5f, 10, null, bulletSpr_norm, 360 - aroundShotStart);
                    break;
            }

        }
    }*/

    void Fire_nShot(float targetTheta, int n, int isPre, float bulletSpeed)
    {
        float ct = targetTheta;

        if (isPre == 3)
        {
            float scaler = Random.Range(0.2f, 1.0f);
            ct += OM.GetPsc().NORMAL_SPEED * OM.GetPsc().direction * scaler;
        }

        if (n == 1)
        {
            LinearShot(EBulletScr.TYPE_NORM, ct, bulletSpeed, 10, null, bulletSprs[0]);
        }
        else if (n == 2)
            for (int d = -1; d <= 1; d += 2)
            {
                LinearShot(EBulletScr.TYPE_NORM, ct + d * 30, bulletSpeed, 10, null, bulletSprs[0]);
            }
        else if (n == 3)
            for (int d = -1; d <= 1; d++)
            {
                LinearShot(EBulletScr.TYPE_NORM, ct + d * 50, bulletSpeed, 10, null, bulletSprs[0]);
            }

    }

    void SelectSadFace()
    {
        int select = Random.Range(0, 3);
        if (faces[select].state == face_state.sad)
        {
            if (select < 2) select++;
            else select--;
        }

        for (int i = 0; i < 3; i++)
        {
            if (i == select)
                Timing.RunCoroutine(ChangeFaceState(i, face_state.sad, true));
            else
                Timing.RunCoroutine(ChangeFaceState(i, face_state.so));
        }

    }

    void UpdateRandomFace()
    {
        List<face_state> fs = new List<face_state>();
        fs.Add(face_state.sad);
        fs.Add(face_state.upset);
        fs.Add(face_state.so);

        for (int i = 0; i < 3; i++)
        {
            int idx = Random.Range(0, 3 - i);
            ChangeFaceState(i, fs[idx]);

            fs.RemoveAt(idx);

        }
    }

    IEnumerator<float> ChangeFaceState(int idx, face_state fs, bool isShake = false)
    {
        // quest :: Face State 바뀔때 애니메이션
        faces[idx].state = fs;
        if (isShake)
        {
            GameObject obj = faces[idx].faceObj;
            Sequence so = DOTweenCustom.ShakeObject(obj.transform, 0.1f, 6, 0.25f, true);

            yield return Timing.WaitUntilDone(DOTweenCustom.WaitForTween(so));
            //yield return Timing.RunCoroutine(DOTweenCustom.shakeObject(obj.transform, 0.1f, 6, 0.25f, true));

        }
        UpdateFaceState(idx);

        yield return Timing.WaitForOneFrame;
    }

    void UpdateFaceState(int idx)
    {
        face_state fs = faces[idx].state;

        Sprite fsprite = FACE_SO;
        switch (fs)
        {
            case face_state.sad:
                fsprite = FACE_SAD;
                break;
            case face_state.so:
                fsprite = FACE_SO;
                break;
            case face_state.upset:
                fsprite = FACE_UPSET;
                break;
        }

        faces[idx].renderer.sprite = fsprite;
    }

    int FaceAtTheta(float theta)
    {
        for (int i = 0; i < faces.Length; i++)
        {
            float left = ThetaRange(faces[i].frontTheta - 60);
            float right = ThetaRange(faces[i].frontTheta + 60);

            if (theta >= left && theta < right)
                return i;
            else
            {
                if (left > right)
                    if (theta >= left || theta < right)
                        return i;
                continue;
            }
        }
        return -1;
    }

    void ChangeFront(int front)
    {
        frontFace = front;
        changeTime = 0;
    }

    void UpdateFrontState()
    {
        for (int i = 0; i < 3; i++)
        {
            faces[i].frontTheta = ThetaRange(currentTheta + 120 * i);
        }

        changeTime += Time.deltaTime;
        if (CheckFront() != frontFace)
            ChangeFront(CheckFront());
    }


    // Update is called once per frame
    new void Update()
    {
        base.Update();
        UpdateFrontState();
        currentTheta = transform.rotation.eulerAngles.z;

        for (int i = 0; i < faces.Length; i++)
        {
            Face cur = faces[i];
            if (!cur.hitTimer.IsStop())
            {
                cur.hitTimer.Update(Time.deltaTime);
                if (cur.hitTimer.IsStop())
                    UpdateFaceState(i);
            }
        }

        shield.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 0, 1), -45 * Time.deltaTime);
    }


    protected override bool OnHit(Collider2D other)
    {
        Vector2 oPos = other.transform.position;
        float oTheta = SM.WhereTheta(oPos.x, oPos.y);
        int hit = FaceAtTheta(oTheta);
        Face hitFace = faces[hit];

        Sprite prevSpr = hitFace.renderer.sprite;
        hitFace.renderer.sprite = FACE_HIT;
        hitFace.hitTimer.resetTimer();

        if (pattern == 2 && hitFace.state == face_state.sad)
        {
            int dmg = ObjectManagerScr.Instance.GetPsc().currentDamage;
            Damaged(dmg * 2);
            return true;
        }
        return false;
    }

    int CheckFront()
    {
        PlayerScr ps = ObjectManagerScr.Instance.GetPsc();
        float t = currentTheta - ps.currentTheta;
        if (t < 0) t = t + 360;

        if (t >= 300 || t <= 60)
        {
            return 0;
        }
        else if (t > 60 && t <= 180)
        {
            return 2;
        }
        else if (t > 180 && t < 300)
        {
            return 1;
        }
        return 0;

    }

    public override void OnEventOccur()
    {
        base.OnEventOccur();
        blockController.PauseAll();
    }

    public override void OnEventResume()
    {
        base.OnEventResume();
        blockController.ResumeAll();
    }


}
