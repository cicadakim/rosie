﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using MEC;
using DG.Tweening;

public class Boss2Scr : Boss {

    const int MAX_SETELLITE = 10;

    public GameObject face;

    public Sprite normalFaceSpr;
    public Sprite damageFaceSpr;

    public GameObject setelliteObj; // 위성 프리팹
    ScrPool<SetelliteScr> setellitePool;
    public Sprite[] setellite_spr_list;
    Timer setelliteTimer;
    List<float> emptySetelliteScrTheta; // 비어있는곳에 위성생성을 위함

    List<SetelliteScr> setelliteList;

    public GameObject vibBullet;
    VibSetter vibSetter;

    protected override void AwakeBossProperty()
    {
        hintStr = "Prevent that Setellites are stacked up";
    }

    protected override void InitBossProperty()
    {
        setellitePool = new ScrPool<SetelliteScr>(setelliteObj, transform, MAX_SETELLITE);
        setelliteList = new List<SetelliteScr>();
        setelliteTimer = new Timer();
        

        emptySetelliteScrTheta = new List<float>(MAX_SETELLITE);
        for(int next=0; next<360; next+= 360/MAX_SETELLITE)
        {
            emptySetelliteScrTheta.Add(next);
        }

    }

    protected override void InitBulletTypes()
    {
        shooter.InitBulletType(EBulletScr.TYPE_VIB, vibBullet, 20);
        vibSetter = new VibSetter();
        shooter.InitBulletType(EBulletScr.TYPE_NORM, normBullet, 20);
    }

    public override void OnGameStart()
    {
        base.OnGameStart();

        StartPattern(1, DoPattern1());
    }

    IEnumerator<float> DoPattern1()
    {
        if (level == 1)
        {
            Shooting(Shooting1, 1.5f);
            setelliteTimer.SetTimer(3);
        }
            
        else if (level == 0)
        {
            Shooting(Shooting1, 2.0f);
            setelliteTimer.SetTimer(4.5f);
        }
        
        while(true)
        {
            UpdateSetellites(Time.deltaTime);
            yield return 0;
            if (isNextBound)
            {
                StartPattern(2,DoPattern2());
                isNextBound = false;
                break;
            }
        }
    }

    IEnumerator<float> DoPattern2()
    {
        
        ClearShootingPattern();
        vibSetter.InitSetter(45, 0.5f);
        if (level == 1)
        {
            setelliteTimer.SetTimer(3);
            Shooting(Shooting2, 1.5f);
        }
        else if (level == 0)
        {
            setelliteTimer.SetTimer(4.5f);
            Shooting(Shooting2, 2.0f);
        }
            

        
        while(true)
        {
            UpdateSetellites(Time.deltaTime);
            yield return 0;
            if (isNextBound)
            {
                StartPattern(3,DoPattern3());
                isNextBound = false;
                break;
            }
        }
    }

    IEnumerator<float> DoPattern3()
    {
        
        ClearShootingPattern();
        if (level == 1)
        {
            setelliteTimer.SetTimer(3.5f);

            Shooting(Shooting3, 2.0f);
            Shooting(ShootToPlayer, 0.5f);
        }
        else if (level == 0)
        {
            setelliteTimer.SetTimer(5.0f);
            Shooting(Shooting3, 3.0f);
            Shooting(ShootToPlayer, 0.8f);
        }
            
        while (true)
        {
            UpdateSetellites(Time.deltaTime);
            yield return 0;
        }

    }
    
    
    void UpdateSetellites(float dt)
    {
        // 기본적으로 setellite는 언제나 생성중. 패턴마다 setelliteTimer를 조정해서 주기조정을 하자
        if (isEnd)
            return;

        float dtime = dt;

        if (setelliteTimer.IsStop())
        {
            if (setellitePool.GetSize() < MAX_SETELLITE)
            {
                if (WakeSetellite())
                    setelliteTimer.resetTimer();
            }

        }
        setelliteTimer.Update(dtime);
        
        for(int i=0; i<setelliteList.Count; i++) 
        {
            SetelliteScr s = setelliteList[i];
            if (s.IsDead())
            {
                SleepSetellite(s); // Quest :: dead처리 callback으로하자
                Damaged(s.MAX_HP / 2);
                setelliteList.RemoveAt(i--);
                continue;
            }
        }
    }

    protected override void OnLose()
    {
        base.OnLose();
        for (int i = 0; i < setelliteList.Count; i++)
        {
            SetelliteScr s = setelliteList[i];
            s.Dead();
        }
    }


    void ShootToPlayer()
    {
        if (level == 0)
            shooter.Shot(EBulletScr.TYPE_NORM, target.transform.eulerAngles.z, transform.position, target.transform.position, 5.0f, 10, null, bulletSprs[1]);
        else if (level == 1)
            shooter.Shot(EBulletScr.TYPE_NORM, target.transform.eulerAngles.z, transform.position, target.transform.position, 5.5f, 10, null,bulletSprs[1]);
    }

    void Shooting1()
    {
        if (level == 0)
            Timing.RunCoroutine(Shot_nWay(EBulletScr.TYPE_NORM, target, 60, 6, 4, 0.5f, 10, 5.0f, null, bulletSprs[0]), "shooting");
        else if(level==1)
            Timing.RunCoroutine( Shot_nWay(EBulletScr.TYPE_NORM, target, 30, 6, 4, 0.5f, 10, 5.0f, null, bulletSprs[0]),"shooting");
    }

    void Shooting2()
    {
        if(level==0)
        {
            Timing.RunCoroutine(Shot_nWay(vibBulletScr.TYPE_VIB, target, 60, 6, 4, 1f, 5, 5.0f, vibSetter, bulletSprs[0]), "shooting");
        }
        else if(level==1)
            Timing.RunCoroutine(Shot_nWay(vibBulletScr.TYPE_VIB, target, 30, 6, 4, 0.5f, 10, 5.0f, vibSetter, bulletSprs[0]), "shooting");
    }

    void Shooting3()
    {
        if(level ==0)
            Timing.RunCoroutine(Shot_nWay(EBulletScr.TYPE_NORM, target, 90, 4, 5, 0.4f, 7, 4.5f, null, bulletSprs[0]), "shooting");
        else if(level==1)
            Timing.RunCoroutine(Shot_nWay(EBulletScr.TYPE_NORM, target, 45, 4, 10, 0.2f, 7, 4.5f, null, bulletSprs[0]), "shooting");
    }

    protected override void AfterDamaged(float dmg)
    {
        base.AfterDamaged(dmg);
        Timing.RunCoroutine(HitFace());
        
    }

    IEnumerator<float> HitFace()
    {
        face.gameObject.GetComponent<SpriteRenderer>().sprite = damageFaceSpr;
        yield return Timing.WaitForSeconds(0.1f);
        face.gameObject.GetComponent<SpriteRenderer>().sprite = normalFaceSpr;
    }

    void SleepSetellite(SetelliteScr s)
    {
        s.gameObject.SetActive(false);
        setellitePool.SleepObject(s);
        emptySetelliteScrTheta.Add(s.frontTheta);
    }

    bool WakeSetellite()
    {
        if (emptySetelliteScrTheta.Count <= 0)
            return false;

        int idx = Random.Range(0, emptySetelliteScrTheta.Count - 1);
        float the = emptySetelliteScrTheta[idx];
        Vector2 pos = SM.WhereInDegreeFromStart(the,2.1f);

        SetelliteScr s = setellitePool.AwakeObject();
        s.gameObject.transform.position = new Vector3(0, 0, 0);
        s.gameObject.transform.rotation = Quaternion.Euler(0, 0, the + 270);
        s.gameObject.SetActive(true);

        setelliteList.Add(s);
        s.InitSetellite(the, pos, 270 , bulletGenPoint );
       
        emptySetelliteScrTheta.RemoveAt(idx);

        return true;
    }

    protected override bool OnHit(Collider2D other)
    {

        return false;
    }
    
}
