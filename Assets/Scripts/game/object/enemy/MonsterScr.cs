﻿using UnityEngine;
using System.Collections;

public class MonsterScr: MonoBehaviour {
    int hp;
    float birthTime;
    public SpriteRenderer spr;
    // Use this for initialization
    void Start () {
        hp = 1;
        birthTime = 0;
    }
	
	// Update is called once per frame
	void Update () {
        birthTime += Time.deltaTime;
	}

    void OnTriggerStay2D(Collider2D other)
    {

        if (birthTime < 0.1f) return;

        switch(other.tag)
        {
            case "PlayerShot":
                hp -= 1;
                Color nc = spr.color;
                nc.r = 0;
                spr.color = nc;
                if(hp<=0)
                    Destroy(gameObject);
                break;

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "BackGround":
                Destroy(gameObject);
                break;

        }
        
    }
}
