﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using MEC;

public class PlayerScr : Unit {

    private const float RECOVER_SPEED = 3.5f;
    public float COMBO_REMAIN_TIME = 7.0f;
    public float NORMAL_SPEED = 60.0f; // 1초에 이동하는 각도(degree)
    
    public float currentSpeed;
    public float currentTheta;

    int walkIdx = 0;
    
    Camera mainCamera;

    public EffectScr healEffect;
    public GameObject hit_effect;
    
    BarController hpController;

    public GameObject background;

    public GameObject shield;

    public Sprite[] normalComboSpr;
    public Sprite normalbulletSpr;
    public Sprite strongbulletSpr;
    public Sprite maxbulletSpr;

    [HideInInspector]
    public Shooter shooter;
    ScrPool<EffectScr> shotEffectPool;

    public GameObject bullet;
    public Transform bulletGenPoint;
    
    public float shotSpeed;
    float shotCool;

    float mouseDownTime;
    bool isMouseDown;

    Timer bindTimer;
    bool isStop; 

    public int DAMAGE; // 초기 damage
    public int currentDamage; // 현재 damage
    [HideInInspector]
    public int direction;
    [HideInInspector]
    public bool isShooting;
    [HideInInspector]
    public int combo;
    int laserFire; // 강한공격

    Sequence comboTxtBlinkTween;
    CoroutineHandle comboFadeCor;
    public Transform comboNumPos;
    public CanvasGroup comboCG;
    public Image comboTxt;
    public GameObject comboScaler;
    NumberScr comboNum;
    Sequence comboTxtFadeTween;

    bool isInvincible;

    bool flip;
    
    NumberManagerScr NM;
    
    SpriteRenderer spriteRenderer;
    Rigidbody2D rb;
    
    public ItemEffectScr itemEffect;

    Vector2 target;

    Animator animator;

    public Sprite[] walkAnimSet;
    public Sprite[] hitAnimSet;
    Sprite[] currentAnimSet;

    public AudioClip shotSFX;
    public AudioClip hitSFX;

    bool isEnd = false;

    void InitPlayer()
    {
        target = new Vector2(0, 0);

        direction = 1;
        hp = MAX_HP;
        currentSpeed = NORMAL_SPEED;
        currentTheta = 0;
        flip = true;
        combo = 0;
        laserFire = 0;
        isShooting = false;
        currentDamage = DAMAGE;

        isInvincible = false;
        isStop = false;

        isMouseDown = false;
        bindTimer = new Timer();
        
        comboTxtFadeTween = DOTween.Sequence().SetAutoKill(false) // using it again after with restart()
            .AppendCallback(() => comboScaler.gameObject.SetActive(true))
            .Append(comboScaler.transform.DOScale(new Vector3(1.4f, 1.4f, 1), 0.3f))
            .Append(comboScaler.transform.DOScale(new Vector3(1.2f, 1.2f, 1), 0.2f))
            .Append(comboCG.DOFade(0.2f, COMBO_REMAIN_TIME - 2) // 2초동안 반짝거림
            );
        comboTxtFadeTween.Complete();
        comboScaler.gameObject.SetActive(false);

        currentAnimSet = walkAnimSet;
    }

    void InitComponent()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void InitShot()
    {
        shotCool = shotSpeed;

        shooter = new Shooter(bulletGenPoint);
        shooter.InitBulletType(PBulletScr.TYPE_PBULLET, bullet, 10);
        shotEffectPool = new ScrPool<EffectScr>(hit_effect,OM.gameObject.transform,10);
    }
    
	// Use this for initialization
	new void Awake() {
        base.Awake();
        InitComponent();
    }
    void Start()
    {
        InitPlayer();
        InitAfterStart();
        InitShot();
    }

    public void OnGameStart()
    {
        hpController = new BarController(GameObject.Find("php_scaler").transform, GameObject.Find("php_gauge").transform);
        isShooting = true;
    }

    public void StartGame()
    {

    }

    void InitAfterStart()
    {
        NM = NumberManagerScr.Instance;
        
        comboNum = NM.CreateNumber(0,comboNumPos.position.x, comboNumPos.position.y,0,null,comboNumPos);

        mainCamera = Camera.main;
    }
	
	// Update is called once per frame
	void Update () {

        if (Time.timeScale == 0 || GM.eventOccur || isEnd) return;

        if (isMouseDown) mouseDownTime += Time.deltaTime;
        
        Move();
        
        AutoShot();
        
    }
    
    void Stop(bool stp)
    {
        isStop = stp;
        if (isStop)
            currentSpeed = 0;
    }
    

    void Move(bool isNuckback = false)
    {
        if (isStop) return;
        if( !bindTimer.IsStop() )
        {
            bindTimer.Update(Time.deltaTime);
            return;
        }
        float addRotate = currentSpeed * Time.deltaTime * direction;
        if (isNuckback)
        {
            addRotate += -10*direction;
            direction *= -1;
        }

        transform.RotateAround(Vector3.zero,Vector3.forward,addRotate);
        mainCamera.transform.RotateAround(Vector3.zero, Vector3.forward, addRotate);

        currentTheta = transform.rotation.eulerAngles.z;
        recoverySpeed(Time.deltaTime);

    }

    /// <summary>
    /// 방향전환 후 스피드 복구
    /// </summary>
    void recoverySpeed(float dt)
    {
        if (isStop) return;
        if (currentSpeed < NORMAL_SPEED)
        {
            currentSpeed += (NORMAL_SPEED * RECOVER_SPEED) * dt;
            if (currentSpeed >= NORMAL_SPEED) currentSpeed = NORMAL_SPEED;
        }
    }
    
    void ShotBulletTo(float speed, float damage, Sprite bulletSpr=null)
    {
        shooter.Shot(PBulletScr.TYPE_PBULLET,currentTheta, transform.position, target, speed, damage,null,bulletSpr);
    }

    void AutoShot()
    {
        if (!isShooting) return;
        if (shotCool >= shotSpeed)
        {
            Timing.RunCoroutine(ShotThree());
            shotCool = 0;
        }
        shotCool += Time.deltaTime;
    }

    IEnumerator<float> ShotThree()
    {
        //SoundManager.PlaySFX(shotSFX);
        for(int i=0; i<3; i++)
        {
            Sprite s = normalbulletSpr;
            if (combo > 1)
                s = normalComboSpr[combo - 1];

            ShotBulletTo(7, currentDamage,s);
            yield return Timing.WaitForSeconds(0.2f);
        }
    }
    
    public void ChangeDirection()
    {
        if (isEnd) return;
        direction = -1*direction;
        currentSpeed = 0;
        spriteRenderer.flipX = (flip = !flip);
    }
    
    
    public void GetEnergy()
    {
        bool isComboIncrease = true;
        if (combo >= 5) // 맥스까지.
        {
            isComboIncrease = false;
            if (laserFire>=5)
            {
                ShotBulletTo(6,currentDamage*6,maxbulletSpr);
                combo = 0;
                laserFire = 0;
                return;
            }
            laserFire++;
            ShotBulletTo(6,currentDamage*4,strongbulletSpr);
        }
        if(isComboIncrease)
        {
            combo = combo + 1;
            comboNum.ChangeNum(combo);
        }

        UpdateDamage();

        if (comboFadeCor.IsValid)
            Timing.KillCoroutines(comboFadeCor);
        
        comboFadeCor = Timing.RunCoroutine(ComboFade());
    }

    IEnumerator<float> ComboFade()
    {
        if (comboTxtBlinkTween!=null && comboTxtBlinkTween.IsPlaying())
            comboTxtBlinkTween.Kill();

        yield return 0;
        comboScaler.gameObject.SetActive(true);
        comboTxtFadeTween.Restart();
        while (!comboTxtFadeTween.IsComplete())
            yield return 0;

        comboTxtBlinkTween = DOTween.Sequence().SetLoops(-1)
            .Append(comboCG.DOFade(0, 0)).AppendInterval(0.1f)
            .Append(comboCG.DOFade(0.2f, 0)).AppendInterval(0.1f);
        // 깜빡이는용
        yield return Timing.WaitForSeconds(2.0f);

        bool lastTime = false; // 시간 다 지났는데 에너지 위에 있음
        int cIdx = 0;
        if(SM.IsOnEnergy())
        {
            lastTime = true;
            cIdx = SM.OnSection();
        }

        while(lastTime)
        {
            if (cIdx == SM.OnSection())
                yield return 0;
            else
                break;
        }
        comboScaler.gameObject.SetActive(false);
        comboTxtBlinkTween.Kill();
        //시간 지남 && 에너지 밖임 == 초기화
        combo = 0;
        laserFire = 0;
        comboNum.ChangeNum(combo);
        UpdateDamage();
        yield break;
    }

    void UpdateDamage()
    {
        currentDamage = DAMAGE * (combo + 1);
    }

    /// <summary>
    /// Item 먹었을때 효과처리
    /// </summary>
    public void GetItem(SectionManagerScr.item_category item)
    {
        switch (item)
        {
            case SectionManagerScr.item_category.heal:
                GetHeal();
                break;
            case SectionManagerScr.item_category.pause:
                itemEffect.StartEffect(SectionManagerScr.item_category.pause, ItemEffectScr.effecttype.item);
               break;
            case SectionManagerScr.item_category.shield:
                ActiveShield(3);
                break;
            case SectionManagerScr.item_category.speed:
                currentSpeed += 5;
                itemEffect.StartEffect(SectionManagerScr.item_category.speed, ItemEffectScr.effecttype.item);
                break;
        }
    }
    
    void DamagedBackground()
    {
        Sequence bg = DOTween.Sequence();
        

        //ActionInstant act = new ActionSequence( new ActionInstant[] { new ActionSetTint(new Vector4(180,0,0,255)),new ActionTintTo(new Vector4(255, 255, 255,255), 0.5f) });
        //actionManager.AttachAction(act,background,"tintBackgroundWhenDamagedAct");
    }

    
    public bool GetHeal() 
    {
        if (hp >= MAX_HP) return false;
        Recovery(10);
        healEffect.StartEffect("effect_heal");
        return true;
    }
    
    public void Recovery(float dHp)
    {
        ChangeHp(hp+dHp);
    }

    public void Bind(float time)
    {
        bindTimer.SetTimer(time);
    }

    void ActiveShield(float time)
    {
        shield.SetActive(true);
        Timing.RunCoroutine(CheckShield(time));
    }

    void Shield(float time,bool isBlank=true)
    {
        if(isBlank)
            DOTweenCustom.Blink(spriteRenderer, 4, time / 4); // 시간이랑 sync맞는지 확인
        
    }

    IEnumerator<float> CheckShield(float time)
    {
        isInvincible = true;
        yield return Timing.WaitForSeconds(time);
        shield.SetActive(false);
        isInvincible = false;
        yield return 0;
    }

    /// <summary>
    /// hit 애니메이션 검사
    /// </summary>
    IEnumerator<float> CheckInvincible()
    {
        yield return 0f;
        while (anim.GetCurrentAnimatorStateInfo(0).IsName("hit1"))
            yield return 0f;
        if(!shield.activeSelf)
            isInvincible = false;
    }
    
    protected override void AfterChangeHp()
    {
        hpController.UpdateCurrentHP(hp, MAX_HP);
    }

    public void OnRemoveBullet(GameObject bullet)
    {
        EffectScr eff = shotEffectPool.AwakeObjectAt(bullet.transform.position,bullet.transform.eulerAngles.z);
        eff.SetOnEnd(()=> shotEffectPool.SleepObject(eff));
        eff.StartEffect("effect_hit");
    }
    
    public override void EnterAttack(float damage, Collider2D other = null)
    {
        if (isInvincible || isEnd) return;
        SoundManager.PlaySFX(hitSFX);  
        Damaged(damage);
    }
    
    protected override void AfterDamaged(float dmg)
    {
        if (hp <= 0)
        {
            if (Application.platform == RuntimePlatform.Android && SceneManagerScr.instance.isVibeOn)
                Handheld.Vibrate();
            GM.GoEnd(false);
            return;
        }
        if(dmg>=10) // quest :: 도트뎀에 적용하지 않기위해서 이렇게 한거긴한데.. 더 깔끔한 방법좀
        {
            DamagedBackground();
            //Invincible(0.8f);

            
            if(Application.platform == RuntimePlatform.Android && SceneManagerScr.instance.isVibeOn)
                Handheld.Vibrate();
            isInvincible = true;
            anim.SetTrigger("hit1");
            Timing.RunCoroutine(CheckInvincible());
            //SoudManagerScr.instance.PlayHit();
        }
    }

    public override void OnEventOccur()
    {
        base.OnEventOccur();

        if (anim != null)
            anim.speed = 0;
        DOTween.Pause(gameObject);
        comboTxtFadeTween.Pause();
        comboTxtBlinkTween.Pause();
    }

    public override void OnEventResume()
    {
        base.OnEventResume();
        if (anim != null)
            anim.speed = 1;
        DOTween.Play(gameObject);
        comboTxtFadeTween.Play();
        comboTxtBlinkTween.Play();
    }

    //이겼을때 이벤트
    public IEnumerator EventWin()
    {
        isEnd = true;

        spriteRenderer.DOFade(1, 0);
        spriteRenderer.sortingOrder = 9;
        //yield return new WaitForSeconds(0.2f);

        animator.speed = 0;

        //yield return new WaitForSeconds(1.0f);
        //float wait = 1f;
        Time.timeScale = 0;
        while (Time.timeScale < 0.5f)
        {
            Time.timeScale += Time.unscaledDeltaTime / 3.0f;
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }

        Time.timeScale = 1;


        animator.speed = 1;
        animator.SetTrigger("win");
        yield return null;
    }

    //졌을때 이벤트 -> 애니메이션 끝날때까지 코루틴
    public IEnumerator EventLose()
    {
        isEnd = true;
        spriteRenderer.DOFade(1, 0);
        spriteRenderer.sortingOrder = 9;
        animator.speed = 0;

        //yield return new WaitForSeconds(1.0f);
        //float wait = 1f;
        Time.timeScale = 0;
        while(Time.timeScale<0.5f)
        {
            Time.timeScale += Time.unscaledDeltaTime/3.0f;
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }

        Time.timeScale = 1;

        yield return new WaitForSeconds(1.0f);

        animator.SetTrigger("lose");
        animator.speed = 1;
        Time.timeScale = 1;
        while (animator.GetCurrentAnimatorStateInfo(0).IsName("lose"))
            yield return null;
    }
    
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Home")
        {
            SM.ActiveHealingAtHome();
        }
    }
    
}
