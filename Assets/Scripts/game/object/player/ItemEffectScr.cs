﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class ItemEffectScr : MonoBehaviour {

    public enum effecttype{ item , heal };

    public Sprite EFF_PAUSE;
    public Sprite EFF_SHIELD;
    public Sprite EFF_SPEED;
    public Sprite EFF_HEAL;

    SpriteRenderer pRenderer;
    SpriteRenderer sRenderer;

    Vector2 originPos;

    void Awake()
    {
        sRenderer = GetComponent<SpriteRenderer>();
        originPos = transform.localPosition;
        gameObject.SetActive(false);
        pRenderer = ObjectManagerScr.Instance.GetPsc().GetComponent<SpriteRenderer>();
    }
    
    Sprite GetSprite(SectionManagerScr.item_category ic)
    {
        Sprite spr = null;
        switch (ic)
        {
            case SectionManagerScr.item_category.heal :
                spr = EFF_HEAL;
                break;
            case SectionManagerScr.item_category.pause:
                spr = EFF_PAUSE;
                break;
            case SectionManagerScr.item_category.shield:
                spr = EFF_SHIELD;
                break;
            case SectionManagerScr.item_category.speed:
                spr = EFF_SPEED;
                break;

        }
        return spr;
    }

    Sequence SetEffect(effecttype type)
    {
        Sequence effect = DOTween.Sequence();
        switch (type)
        {
            case effecttype.heal:
                sRenderer.DOFade(0, 0);
                effect.Append( sRenderer.DOFade(1,0.6f) );
                effect.Append(sRenderer.DOFade(0, 0.4f));

                break;
            case effecttype.item:
                sRenderer.DOFade(0, 0);
                effect.Append(sRenderer.DOFade(1, 0.6f));
                effect.Append(sRenderer.DOFade(0, 0.4f));
                effect.Join(transform.DOMoveY(1, 0.4f).SetRelative());
                break;
        }
        effect.OnComplete(() => gameObject.SetActive(false));
        return effect;
    }

    public void StartEffect(SectionManagerScr.item_category ic, effecttype type,bool back=false)
    {
        
        Sprite spr = GetSprite(ic);
        Sequence effect = SetEffect(type);

        if (spr == null || !effect.IsPlaying()) return;

        if (back)
            sRenderer.sortingOrder = pRenderer.sortingOrder-1;
        else
            sRenderer.sortingOrder = pRenderer.sortingOrder+1;

        GoInit(); 

        sRenderer.sprite = spr;
        gameObject.SetActive(true);
    }

    void GoInit()
    {
        sRenderer.color = new Color(1, 1, 1, 1);
        transform.localPosition = originPos;
        StopAllCoroutines();
    }
    

}
