﻿using System.Collections.Generic;
using UnityEngine;

class DamageOption : WeaponOption
{
    int damage = 0;
    PlayerScr psc = ObjectManagerScr.Instance.GetPsc();
    public DamageOption(int dmg)
    {
        damage = dmg;
    }
    protected override void DoOnCollideOnChild(Collider2D other)
    {
        psc.EnterAttack(damage);
    }
}