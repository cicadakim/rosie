﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LinearMove : MoveOption
{
    Vector2 direction;
    Vector2 dest;

    public LinearMove(Vector2 dest, float speed)
    {
        this.speed = speed;
        this.dest = dest;
    }

    protected override void AfterSetBody()
    {
        direction = (dest - body.position).normalized;
    }

    public LinearMove Clone()
    {
        return new LinearMove(dest, speed);
    }
    
    public override void Move(float dt)
    {
        Vector2 destAdd = direction * speed * dt;
        Vector2 pos = (Vector2)body.position + destAdd;
        body.MovePosition(pos);
    }
}

