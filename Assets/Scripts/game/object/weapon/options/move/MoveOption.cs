﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class MoveOption
{
    protected Rigidbody2D body;
    public float speed;

    /// <summary>
    /// move전 반드시 body를 설정해줘야함
    /// </summary>
    public void SetBody(GameObject obj)
    {
        body = obj.GetComponent<Rigidbody2D>();
        AfterSetBody();
    }


    protected virtual void AfterSetBody() { }

    public abstract void Move(float dt);
}

