﻿using UnityEngine;
using System.Collections.Generic;

public abstract class WeaponOption
{
    protected abstract void DoOnCollideOnChild(Collider2D other);
    public void DoOnCollide(Collider2D other)
    {
        DoOnCollideOnChild(other);
    }

}

