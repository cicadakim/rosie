﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Shooter
{
    protected Dictionary<int,List<BulletScr>> bulletLists; // 활성화된 총알 리스트
    protected Dictionary<int,BulletPool> bulletPools;
    Transform bulletGen;

    List<int> typeList;
    
    /// quest:: prefab bullet을 받는데 bullet종류마다 prefab다르게 줄거 아니면 scr만 교체하는것도 나쁘지 않을듯.
    public Shooter(Transform bulletGenPoint)
    {
        bulletGen = bulletGenPoint;
        bulletPools = new Dictionary<int,BulletPool>();
        bulletLists = new Dictionary<int, List<BulletScr>>();
        typeList = new List<int>();
    }

    public bool InitBulletType(int bullet_type,GameObject pref,int n=0)
    {
        if (typeList.IndexOf(bullet_type) != -1) return false; // 이미 있음
        bulletPools[bullet_type] = new BulletPool(pref, bulletGen,this,n);
        
        bulletLists[bullet_type] = new List<BulletScr>();
        typeList.Add(bullet_type);
        return true;
    }
    
    public BulletScr WakeBullet(float theta,Vector2 from,int bullet_type, Sprite bulletSpr = null)
    {
        if (typeList.IndexOf(bullet_type) == -1) return null; // quest :: 없으면 initBullet해줄까? 이거 하려면 type으로 프리팹을 알아내야함. 이렇게 구현해도 괜찮을거같긴함
        
        BulletScr bs = bulletPools[bullet_type].AwakeObjectAt(from,theta);

        bs.SetShooter(this);
        if (bulletSpr!=null)bs.SetSprite(bulletSpr);
        bulletLists[bullet_type].Add(bs);
        return bs;
    }

    public void Shot(int type,float theta, Vector2 from, Vector2 to, float speed, float damage, BulletSetter bst,Sprite bulletSpr = null)
    {
        BulletScr bullet = WakeBullet(theta,from,type, bulletSpr);
        if(bst!=null)bst.SetBullet(bullet);
        bullet.Fire(from, to, speed, damage);
    }

    public void Shot(int type,float theta, Vector2 from,float time, float damage, BulletSetter bst, Sprite bulletSpr = null)
    {
        BulletScr bullet = WakeBullet(theta, from, type, bulletSpr);
        if (bst != null) bst.SetBullet(bullet);
        bullet.Fire(time, damage);
    }

    public void RemoveBullet(BulletScr bullet)
    {
        bullet.OnRemove();
        int bullet_type = bullet.type;
        if (typeList.IndexOf(bullet_type) == -1) return;
        bulletPools[bullet_type].SleepObject(bullet);
        bulletLists[bullet_type].Remove(bullet);
    }

    public void RemoveAllBullet()
    {
        foreach (int type in bulletPools.Keys)
            RemoveAllBullet(type);
    }

    public void RemoveAllBullet(int bullet_type)
    {
        if (typeList.IndexOf(bullet_type) == -1) return;
        for (int i = 0; i < bulletLists[bullet_type].Count; i++)
        {
            if (!bulletLists[bullet_type][i]) continue;
            bulletPools[bullet_type].SleepObject(bulletLists[bullet_type][i]);
        }
        bulletLists[bullet_type].Clear();
    }


}
