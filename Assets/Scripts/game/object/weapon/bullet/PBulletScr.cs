﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PBulletScr : BulletScr
{
    public AudioClip hitSFX;

    public override void InitProperty()
    {
        base.InitProperty();
        c2d = GetComponent<BoxCollider2D>();
    }

    protected override void TriggerEnterProperty(Collider2D other)
    {
        bool isRemoved = false;
        switch (other.tag)
        {
            case "BossShield":
                isRemoved = true;
                break;
            case "Boss":
                isRemoved = true;
                Boss bossScr = ObjectManagerScr.Instance.GetBsc();
                bossScr.EnterAttack(damage,c2d);
                break;
            case "Enemy":
                isRemoved = true;
                Enemy enm = other.gameObject.GetComponent<Enemy>();
                enm.EnterAttack(damage,c2d);
                break;
        }
        if (isRemoved)
        {
            SoundManager.PlaySFX(hitSFX);
            ObjectManagerScr.Instance.GetPsc().OnRemoveBullet(gameObject);
            shooter.RemoveBullet(this);
        }
    }

}
