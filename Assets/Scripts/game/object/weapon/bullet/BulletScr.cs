﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;
using DG.Tweening;


public class BulletSetter
{
    float damage, speed;
    Sprite spr;

    //Boss에서 InitSetter로 Setter에 값 전달 해주고
    //Shooter에서 Shot하기전에 SetBullet으로 Bullet 전달해주면 초기화
    public virtual void InitSetter(float damage,float speed,Sprite spr){
        this.damage = damage;
        this.speed = speed;
        this.spr = spr;
    }

    public virtual void SetBullet(BulletScr vb){}
}

public class BulletScr : MonoBehaviour
{
    ///types
    ///
    public const int TYPE_PBULLET = 0;
    public const int TYPE_NORM = 10;
    public const int TYPE_GS = 11;
    public const int TYPE_VIB = 12;
    public const int TYPE_PATH = 13;
    public const int TYPE_EASE = 14;


    public float damage;
    public float speed;
    protected Vector2 direction;
    
    protected Rigidbody2D rb2d;
    protected Collider2D c2d;
    public SpriteRenderer sr;

    protected bool isFire;

    protected Shooter shooter;
    public int type = 0;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        c2d = GetComponent<CircleCollider2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    public virtual void InitProperty() {
    }
    
    void Update()
    {
        if (!isFire || GameManagerScr.Instance.eventOccur )
            return;

        Move(Time.deltaTime);
    }

    public void SetShooter(Shooter s)
    {
        this.shooter = s;
    }

    public void SetSprite(Sprite spr)
    {
        if (sr.sprite == spr)
            return;
        sr.sprite = spr;
        //Vector3 sprb = spr.bounds.size; 
        //c2d.radius = sprb.x * 0.9f;
    }

    public virtual void Fire(Vector2 from,Vector2 to,float speed,float damage)
    {
        this.speed = speed;
        this.damage = damage;
        this.direction = (to - from).normalized;
       // transform.DOMove(to, speed).SetSpeedBased().SetEase(Ease.Linear);
        isFire = true;
    }

    public virtual void Fire(float time, float damage)
    {
    }

    protected virtual void Move(float dt)
    {
        Vector2 destAdd = direction * speed * dt;
        Vector2 pos = (Vector2)rb2d.position + destAdd;
        rb2d.MovePosition(pos);
    }

    public void SetFire(bool f)
    {
        isFire = f;
    }

    public virtual void OnRemove()
    {
        DOTween.Kill(gameObject);
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (!gameObject.activeSelf) return;

        TriggerEnterProperty(other);
    }

    protected virtual void TriggerEnterProperty(Collider2D other) { }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!gameObject.activeSelf) return;
        switch (other.tag)
        {
            case "BackGround":
                Timing.RunCoroutine(RemoveBulletWithFade());
                break;
        }
    }
    protected IEnumerator<float> RemoveBulletWithFade()
    {
        c2d.enabled = false;
        yield return Timing.WaitUntilDone(DOTweenCustom.WaitForTween(sr.DOFade(0, 0.2f)));
        shooter.RemoveBullet(this);
        c2d.enabled = true;
        sr.DOFade(1, 0);
    }
}
