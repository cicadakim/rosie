﻿
using System.Collections.Generic;
using UnityEngine;

public class VibSetter : BulletSetter
{
    float degree;
    float time;
    bool leftFirst;
    public void InitSetter(float degree, float time, bool leftFirst = true)
    {
        this.degree = degree;
        this.time = time;
        this.leftFirst = leftFirst;
    }

    public override void SetBullet(BulletScr vb)
    {
        ((vibBulletScr)vb).SetVibe(degree,time,leftFirst);
    }
}


/// <summary>
/// 
/// vibration bullet
/// 
/// </summary>
public class vibBulletScr : EBulletScr
{
    Vector2 leftDir;
    Vector2 rightDir;
    Vector2 moveDir;

    bool isFirst = true;
    float time; // direction time
    float degree;
    bool leftFirst;
    Timer directionTimer;

    public override void InitProperty()
    {
        base.InitProperty();
        type = TYPE_VIB;
    }

    /// <summary>
    /// Fire전에 호출해서 Bullet에 Vibe 특성 부여
    /// </summary>
    public void SetVibe(float degree, float time, bool leftFirst = true)
    {
        this.degree = degree;
        this.time = time;
        this.leftFirst = leftFirst;
        directionTimer = new Timer(time / 2);
    }

    public override void Fire(Vector2 from, Vector2 to, float speed,float damage)
    {
        base.Fire(from, to, speed,damage);

        leftDir = MathComponent.RotateVector(direction, degree);
        rightDir = MathComponent.RotateVector(direction, -degree);

        moveDir = leftDir;
        if (!leftFirst) moveDir = rightDir;
    }
    
    protected override void Move(float dt)
    {
        Vector2 destAdd = moveDir * speed * dt;
        Vector2 pos = (Vector2)rb2d.position + destAdd;
        rb2d.MovePosition(pos);

        if (directionTimer.IsStop())
        {
            if (isFirst)
            {
                isFirst = false;
                directionTimer.SetTimer(time);
            }


            if (moveDir == rightDir)
                moveDir = leftDir;
            else
                moveDir = rightDir;

            directionTimer.resetTimer();
            return;
        }

        directionTimer.Update(dt);

    }


}

