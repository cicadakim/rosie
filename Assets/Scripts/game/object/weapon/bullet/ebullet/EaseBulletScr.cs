﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EaseSetter : BulletSetter
{
    Ease ease;
    AnimationCurve curve=null;
    public void InitSetter(Ease ease)
    {
        this.ease = ease;
    }
    public void InitSetter(AnimationCurve curve)
    {
        this.curve = curve;
    }

    public override void SetBullet(BulletScr pb)
    {
        if(curve!=null) ((EaseBulletScr)pb).SetEase(curve);
        else((EaseBulletScr)pb).SetEase(ease);
    }
}


/// <summary>
/// 
/// vibration bullet
/// 
/// </summary>
public class EaseBulletScr : EBulletScr
{
    bool isCurve = false;
    AnimationCurve curve;
    Ease easeType;
    Tweener moveTween;
    
    public override void InitProperty()
    {
        base.InitProperty();
        type = TYPE_EASE;
    }

    /// <summary>
    /// Fire전에 호출해서 Bullet에 Vibe 특성 부여
    /// </summary>
    /// 
    public void SetEase(Ease ease)
    {
        easeType = ease;
    }
    public void SetEase(AnimationCurve curve)
    {
        isCurve = true;
        this.curve= curve;
    }


    public override void Fire(Vector2 from, Vector2 to, float speed, float damage)
    {
        base.Fire(from, to, speed, damage);
        moveTween = rb2d.DOMove(to, Vector2.Distance(from, to) / speed);
        if (isCurve)
            moveTween.SetEase(curve);
        else
            moveTween.SetEase(easeType);
    }

    protected override void Move(float dt)
    {
    }

    public override void OnRemove()
    {
        if (moveTween.IsPlaying())
            moveTween.Kill();
    }

}

