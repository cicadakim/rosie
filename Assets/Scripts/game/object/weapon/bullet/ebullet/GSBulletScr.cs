﻿
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Go and Stop Bullet.
/// </summary>

class GSBulletScr : EBulletScr
{
    int state = 1; // go==1 stop==-1
    Timer stateTimer;

    public override void InitProperty()
    {
        base.InitProperty();
        stateTimer = new Timer(0.5f);
        type = TYPE_GS;

    }

    protected override void Move(float dt)
    {
        if(state==1) // go
        {
            Vector2 destAdd = direction * speed * dt;
            Vector2 pos = (Vector2)rb2d.position + destAdd;
            rb2d.MovePosition(pos);
        }

        if (stateTimer.IsStop())
        {
            state = -state;
            stateTimer.resetTimer();
            return;
        }
        stateTimer.Update(dt);
    }
}

