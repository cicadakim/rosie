﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using DG.Tweening;

public class EBulletScr : BulletScr
{
    PlayerScr pScr;

    ///EBullet : Init~~()시리즈로 각 총알에 특성 부여 , Fire()로 특정 방향과 속도로 발사 <-- objectPool에서 꺼낸뒤 초기화

    
    public override void InitProperty()
    {
        base.InitProperty();
        pScr = ObjectManagerScr.Instance.GetPsc();
        type = TYPE_NORM;
    }

    protected override void TriggerEnterProperty(Collider2D other)
    {
        switch (other.tag)
        {
            case "Player":
                pScr.EnterAttack(damage,c2d);
                shooter.RemoveBullet(this);
                break;
            case "PlayerShield":
                shooter.RemoveBullet(this);
                break;
        }
    }


}