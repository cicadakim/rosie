﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
class LaserWeapon
{
    RenderPool laserPool;
    SectionManagerScr SM;
    GameManagerScr GM;
    List<WeaponOption> options;

    Vector2 position;

    public LaserWeapon(GameObject l, Transform lp, Vector2 pos, int n = 0)
    {
        position = pos;
        laserPool = new RenderPool(l, lp, n);
        SM = SectionManagerScr.Instance;
        GM = GameManagerScr.Instance;
        options = new List<WeaponOption>(); // laser의 option(감속,데미지 등등)
    }
    
    public void AddOption(WeaponOption opt)
    {
        options.Add(opt); // next shot부터 적용
    }

    public void RemoveOption(WeaponOption opt)
    {
        options.Remove(opt); //next shot부터 적용됨
    }

    public void Shot(float targetTheta, float damage, float waitTime, float remainTime, Sprite waitSpr, Sprite activeSpr)
    {
        Vector2 dest = SM.WhereInDegree(targetTheta);

        float dist = MathComponent.Distance(position, dest);
        float angle = MathComponent.AngleTwoPoint(position, dest);

        dest += position;
        GameObject laser = laserPool.WakeObjectAt(new Vector2(dest.x / 2, dest.y / 2), waitSpr, angle);
        //laser.transform.RotateAround(position, new Vector3(0, 0, 1), targetTheta);

        Vector3 tempScale = laser.transform.localScale;
        tempScale.y = dist;
        laser.transform.localScale = tempScale;

        laser.GetComponent<LaserScr>().ShotWithOptions(damage, options); // 원하는 레이저 옵션으로 쏘도록
        Timing.RunCoroutine(LaserUpdate(laser, activeSpr, waitTime, remainTime));
    }

    IEnumerator<float> LaserUpdate(GameObject laser, Sprite activeSpr, float waitTime, float remainTime)
    {
        float cTime = waitTime;
        while (cTime > 0)
        {
            cTime -= Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }
        laser.GetComponent<LaserScr>().ActiveLaser(activeSpr);
        laser.GetComponent<SpriteRenderer>().DOFade(0, remainTime).OnKill( ()=> SleepLaser(laser) ); // quest :: laser안쪽에서 이걸 해주는게 더나을듯
        yield break;
    }

    void SleepLaser(GameObject laser)
    {
        Vector3 tempScale = laser.transform.localScale;
        tempScale.y = 1;
        laser.transform.localScale = tempScale;
        laser.GetComponent<LaserScr>().ReleaseOptions(); // 옵션 초기화
        laserPool.SleepObject(laser);
    }

}