﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScr : MonoBehaviour {

    List<WeaponOption> options = null;
    Collider2D c2d;
    public SpriteRenderer spriteRenderer;
    public float damage;
    // Use this for initialization
    void Awake() {
        options = null;
        c2d = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
    public void InitOptions(List<WeaponOption> opts)
    {
        options = opts;
    }

    public void ReleaseOptions()
    {
        options = null;
    }

    public void ShotWithOptions(float damage,List<WeaponOption> opts) 
    {
        this.damage = damage;
        InitOptions(opts);
        c2d.enabled = false;
        spriteRenderer.color = new Color(1, 1, 1, 0.5f);
    }

    public void ActiveLaser(Sprite activeSpr) // waitTime이 끝났을때
    {
        spriteRenderer.sprite = activeSpr;
        spriteRenderer.color = new Color(1, 1, 1, 1);
        c2d.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "Player":
                /*
                if (options == null)
                    return;
                foreach (WeaponOption opt in options)
                {
                    Debug.Log("dd");
                    opt.DoOnCollide(other);
                }
                */
                ObjectManagerScr.Instance.GetPsc().EnterAttack(damage);
                break;
        }
        
    }
}
