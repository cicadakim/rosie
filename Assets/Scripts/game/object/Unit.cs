﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// 게임 속 타겟이 될 수 있는 모든 유닛의 상위 클래스
/// </summary>
public class Unit : MonoBehaviour
{
    public float hp;
    public float MAX_HP;

    protected bool isEnd;

    protected GameManagerScr GM;
    protected SectionManagerScr SM;
    protected ObjectManagerScr OM;

    protected Animator anim; // 없는 하위클래스도 있을수있음.. event시 멈춰주기위함. 흠.. 이벤트 너무 지저분한뎅

    protected void Awake()
    {
        GM = GameManagerScr.Instance;
        SM = SectionManagerScr.Instance;
        OM = ObjectManagerScr.Instance;

        anim = GetComponent<Animator>();

        isEnd = false;
    }

    /// <summary>
    /// hp감소, 0이하면 return true
    /// </summary>
    public bool Damaged(float dmg)
    {
        if (CheckInv() || isEnd)
            return false;
        ChangeHp(hp-dmg);
        AfterDamaged(dmg);

        if (hp <= 0) return true;
        return false;
    }

    protected bool ChangeHp(float nHp)
    {
        hp = nHp;
        if (hp > MAX_HP) hp = MAX_HP;
        if (hp <= 0)
        {
            hp = 0;
        }
        AfterChangeHp();
        return hp <= 0;
    }

    /// <summary>
    /// 타격 받을 상황인지 check
    /// </summary>
    protected virtual bool CheckInv() { return false;  }
    protected virtual void AfterChangeHp() { }
    protected virtual void AfterDamaged(float dmg) { }

    /// <summary>
    /// Attack 들어왔을때 대응하는 함수
    /// </summary>
    public virtual void EnterAttack(float damage,Collider2D other=null) { }

    public virtual void OnEventOccur()
    {
        DOTween.Pause(gameObject);
        //if (anim != null)
            //anim.speed = 0;
    }

    public virtual void OnEventResume()
    {
        DOTween.Play(gameObject);
        if (anim != null)
            anim.speed = 1;
    }
}

