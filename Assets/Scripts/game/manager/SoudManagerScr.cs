﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoudManagerScr : MonoBehaviour {

    public static SoudManagerScr instance=null;
    public AudioClip bossHitsound;
    public AudioClip playerHitsound;
    public AudioClip bgSound;
    AudioSource myAudio;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
	// Use this for initialization
	void Start () {
        myAudio = GetComponent<AudioSource>();
	}

    public void PlayHit()
    {
        myAudio.PlayOneShot(playerHitsound);
    }

    public void BossHit()
    {
        myAudio.PlayOneShot(bossHitsound);
    }
    
	
	// Update is called once per frame
	void Update () {
		
	}
}
