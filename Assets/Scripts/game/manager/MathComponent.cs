﻿using UnityEngine;


static public class MathComponent
{
    static public float DtoR = Mathf.PI / 180.0f;
    static public float RtoD = 1.0f/DtoR;

    /// <summary>
    /// 두점을 받아 끼인각 theta반환
    /// </summary>
    public static float AngleTwoPoint(Vector2 src, Vector2 dest)
    {
        float distx = dest.x - src.x;
        float disty = dest.y - src.y;
        float angle = 180.0f / Mathf.PI * Mathf.Atan2(disty, distx);
        angle += 90;

        return angle;
    }

    /// <summary>
    /// src와 dest의 거리
    /// </summary>
    public static float Distance(Vector2 src, Vector2 dest)
    {
        float dx = Mathf.Abs(src.x - dest.x);
        float dy = Mathf.Abs(src.y - dest.y);
        float dist = Mathf.Sqrt(dx * dx + dy * dy);
        return dist;
    }

    /// <summary>
    /// [inclusive]min~[inclusive]max
    /// </summary>
    static bool IsInRange(float target, float min, float max)
    {
        return (target >= min && target <= max);
    }

    /// <summary>
    /// target이 rangeOrigin부터 양옆 range범위만큼 안에 들어가는가
    /// range는 360도까지
    /// </summary>
    public static bool IsInRangeFromOrigin(float target, float rangeOrigin, float range)
    {
        float halfrange = range / 2;
        if (halfrange >= 180) halfrange = 180;

        float minR = rangeOrigin - halfrange;
        float maxR = rangeOrigin + halfrange;
         
        if (minR < 0)
        {
            float minQ = minR + 360;
            if (IsInRange(target, minQ, 360) || IsInRange(target, 0, maxR))
                return true;
        }
        else if (maxR >= 360)
        {
            float maxQ = maxR - 360;
            if (IsInRange(target, minR, 360) || IsInRange(target, 0, maxQ))
                return true;
        }
        else
            return IsInRange(target, minR, maxR);
        return false;
    }

    /// <summary>
    /// 2차원 벡터 src를 degree만큼 회전
    /// </summary>
    public static Vector2 RotateVector(Vector2 src, float degree)
    {
        float r = degree * DtoR;
        return new Vector2(Mathf.Cos(r) * src.x - Mathf.Sin(r) * src.y , Mathf.Cos(r) * src.y + Mathf.Sin(r) * src.x );
    }


    /// <summary>
    /// theta를 0~360범위 안으로 return
    /// </summary>
    public static float ThetaIn2PI(float theta)
    {
        if (theta >= 360) return theta-360;
        if (theta <= 0) return theta + 360;
        return theta;
    }


    /// <summary>
    /// rect안에 pos가 들어가는지 여부 return
    /// </summary>
    public static bool IsIn(Rect rect, Vector2 pos)
    {
        if (rect.x < pos.x && rect.x + rect.width > pos.x && rect.y > pos.y && rect.y - rect.height < pos.y)
            return true;

        return false;
    }
    

}