﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using MEC;

public class SectionManagerScr : MonoBehaviour{

    public enum section_state { sleep, active};
    public enum active_mode { wait,enegy,item, special };
    public enum item_category { shield, pause, speed, heal };

    
    // 각 Section에 있는 아이템,에너지등 active의 정보
    public struct Section
    {
        public GameObject actSign;
        public SpriteRenderer actRend;

        public Timer keepTimer;
        public Timer remainTimer;

        public bool keepEnd;
        public bool remainEnd;
        
        public section_state state;
        public active_mode mode;
        public item_category item;
        public bool isOn;
        public Sprite onSpr;
        public Sprite normalSpr;

        public IEnumerator<float> afterKeepEvent;
        public IEnumerator<float> afterRemainEvent;
        public CoroutineHandle updateEvent; // remain중에 계속 뭔가 하는부류
    }

    //행성 둘레에서 위치나 각도를 계산하기위함
    public float Radius;
    const float DtoR = Mathf.PI / 180.0f;
    const float RtoD = 180.0f / Mathf.PI;

    const int SECTION_NUM = 10;
    const float SECTION_DEG = 360/SECTION_NUM ;

    List<int> sleepSections;
    Section[] sections;

    public float ACTIVE_KEEPTIME;
    public float ITEM_REMAINTIME;
    public float WAIT_KEEPTIME;

    public Sprite SPR_ENERGY_WAIT;
    public Sprite SPR_ENERGY_NORMAL;
    public Sprite SPR_ENERGY_ON;

    public Sprite SPR_ITEM_PAUSE;
    public Sprite SPR_ITEM_SPEED;
    public Sprite SPR_ITEM_SHIELD;

    public Sprite SPR_ITEM_PAUSE_ON;
    public Sprite SPR_ITEM_SPEED_ON;
    public Sprite SPR_ITEM_SHIELD_ON;
   
    public Sprite SPR_TRAP;
    
    int numItem;
    const int maxItem = 3;
    CoroutineHandle itemGenerator;

    public int maxEnergy;

    int onIdx;
    
    int enterDirection;

    public static SectionManagerScr Instance = null;

    public float decSpeed=55;
    public float incSpeed=35;
    
    public GameObject activeSign;

    public SpriteRenderer homeRend;
    public Sprite homeActive;
    public Sprite homeDisactive;

    Timer healTimer;
    
    List<int> waitCallList;

    GameManagerScr GM;
    ObjectManagerScr OM;


    public AudioClip energySFX;


    // SectionManager 기본 init
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        InitSection();
    }

    // Manager들 초기화 뒤에 init
    void Start()
    {
        GM = GameManagerScr.Instance;
        OM = ObjectManagerScr.Instance;
    }

    // SectionManager 기본 init
    void InitSection()
    {
        numItem = 0;
        
        waitCallList = new List<int>();
        sections = new Section[SECTION_NUM];
        sleepSections = new List<int>(SECTION_NUM);

        float startAngle = 18;
        float dAngle = 360 / SECTION_NUM;
        ////make lines and active sign
        for (int i=0; i<SECTION_NUM; i++)
        {
            // make sections
            Vector3 actPosition = WhereInDegree(startAngle+dAngle*i);
            Quaternion rotate = new Quaternion();
            rotate.eulerAngles = new Vector3(0, 0, startAngle+ dAngle * i + 180);
            GameObject acts = Instantiate(activeSign , actPosition , rotate ,gameObject.transform);

            sections[i].actSign = acts;
            sections[i].isOn = false;
            sections[i].actRend = sections[i].actSign.GetComponent<SpriteRenderer>();
            sections[i].keepTimer = new Timer();
            sections[i].remainTimer = new Timer();
            Timing.RunCoroutine(SleepSection(i));
        }
        healTimer = new Timer(30);
        healTimer.StopTimer();
    }

    public void InitEnergyRandomly()
    {
        int sec = 0;
        for (int i = 0; i < maxEnergy; i++)
        {
            sec += SECTION_NUM / maxEnergy;
            ActiveSection(sec, ACTIVE_KEEPTIME, 0, active_mode.enegy, AfterKeepEnergy(sec), null);
        }
    }

    public void GenerateEnergy(int sec)
    {
        ActiveSection(sec, ACTIVE_KEEPTIME, 0, active_mode.enegy, AfterKeepEnergy(sec), null);
    }
    

    void EnterSection(int idx)
    {
        onIdx = idx;
        sections[onIdx].isOn = true;

        if (sections[idx].state != section_state.active || sections[idx].mode == active_mode.wait) return; // active가 아닌곳에 enter시 작용 없음.
        
        sections[idx].actRend.sprite = sections[idx].onSpr;
    }

    void UpdateSection(int idx)
    {
        if (sections[idx].state == section_state.sleep) return;

        if (sections[idx].afterRemainEvent!= null && !sections[idx].remainEnd)
        {
            if (!sections[idx].remainTimer.IsStop())
            {
                sections[idx].remainTimer.Update(Time.deltaTime);
            }
            else // remainTime 끝났을때 event
            {
                if (idx != onIdx) // 올라가 있으면 일단 계속
                {
                    sections[idx].remainEnd = true;
                    Timing.RunCoroutine(sections[idx].afterRemainEvent);
                }
            }
        }

        if (idx == onIdx && sections[idx].afterKeepEvent != null && !sections[idx].keepEnd) // player가 위에서 keeping
        {
            if (!sections[idx].keepTimer.IsStop())
                sections[idx].keepTimer.Update(Time.deltaTime);
            else // keepTime 끝났을때 event
            {
                sections[idx].keepEnd = true; // quest :: 이런방법밖에 없냐..
                Timing.RunCoroutine(sections[idx].afterKeepEvent);
            }
        }
    }
    void ExitSection(int idx)
    {
        sections[idx].isOn = false;
        if (sections[idx].state != section_state.active) return;
        sections[idx].keepTimer.resetTimer();
        sections[idx].actRend.sprite = sections[idx].normalSpr;
        //UpdateActiveColor(idx);
    }

    void Update()
    {
        if (GM.eventOccur)
            return;

        int currentIdx = SectionInTheta( ObjectManagerScr.Instance.GetPsc().currentTheta );
        if(onIdx != currentIdx)
        {
            ExitSection(onIdx); // onIdx를 나가고
            EnterSection(currentIdx); // onIdx <= currentIdx
        }

        for(int i=0; i<SECTION_NUM; i++)
            if (sections[i].state != section_state.sleep) UpdateSection(i);
        
        for (int i = 0; i < waitCallList.Count; i++) // waring :: 이거 wait 뻑갈수도 있음
        {
            if (GenerateWait(waitCallList[i]))
            {
                waitCallList.RemoveAt(i);
                --i;
                continue;
            }
        }

        CheckHealTimer();
        
    }

    void CheckHealTimer()
    {
        if (!healTimer.IsStop())
        {
            healTimer.Update(Time.deltaTime);
        }
        if (healTimer.IsStop())
        {
            homeRend.sprite = homeActive;
        }
    }

    public void StartGenerateItem(float duration)
    {
        itemGenerator = Timing.RunCoroutine(GeneratingItem(duration));
    }

    public void StopGenerateItem()
    {
        Timing.KillCoroutines(itemGenerator);
    }

    /// <summary>
    /// 정해진 시간마다 아이템 생성
    /// </summary>
    IEnumerator<float> GeneratingItem(float duration)
    {
        while(true)
        {
            GenerateItem();
            yield return Timing.WaitForSeconds(duration);
        }
    }
    
    /// <summary>
    /// 랜덤한 위치에 정해진 확률로 아이템 만듦
    /// </summary>
    void GenerateItem()
    {
        Debug.Log(numItem);
        if (sleepSections.Count <= 0) return;
        
        if (numItem >= maxItem) return;

        IEnumerator<float> runItem = null;

        int idx = 0;
        idx = UnityEngine.Random.Range(0,sleepSections.Count);
        idx = sleepSections[idx];
        
        int r = UnityEngine.Random.Range(1, 50);
        
        if (r <= 35)
        {
            sections[idx].item = item_category.shield;
        }
        else
        {
            sections[idx].item = item_category.heal;
        }
        
        if (!ActiveSection(idx,ACTIVE_KEEPTIME,ITEM_REMAINTIME,active_mode.item,RunItem(idx,runItem),SleepItem(idx)))
            return;
        numItem++;
            
        if (idx == onIdx) EnterSection(idx);
        
    }

    bool GenerateWait(int idx)
    {
        int nextIdx = 0;
        int[] addidx = { -3,-2,-1,1,2,3 };
        nextIdx = idx + addidx[UnityEngine.Random.Range(0, 5)];
        if (nextIdx >= SECTION_NUM) nextIdx -= SECTION_NUM;
        if (nextIdx < 0) nextIdx = SECTION_NUM + nextIdx;
        return ActiveSection(nextIdx,0,WAIT_KEEPTIME, active_mode.wait,null, WaitToEnergy(nextIdx));
    }

    /// <summary>
    /// 외부에서 동작을 결정해서 생성
    /// </summary>
    public bool GenerateSpecialAt(int idx, float keeptime, float remaintime,Sprite norm,Sprite on, IEnumerator<float> akEvent, IEnumerator<float> arEvent, IEnumerator<float> uEvent = null)
    {
        bool ret = ActiveSection(idx, keeptime, remaintime, active_mode.special, akEvent, arEvent, uEvent);

        if (ret)
            SetSprOnSection(idx, norm, on);

        return ret;
    }

    public int GetSleepIdx()
    {
        if (sleepSections.Count <= 0) return -1;
        int idx = UnityEngine.Random.Range(0, sleepSections.Count);
        return idx;
    }
    
    void UpdateActiveSpr(int idx)
    {
        if (sections[idx].state != section_state.active) return;
        switch (sections[idx].mode)
        {
            case active_mode.wait:
                sections[idx].normalSpr = SPR_ENERGY_WAIT;
                break;
            case active_mode.enegy:
                sections[idx].normalSpr = SPR_ENERGY_NORMAL;
                sections[idx].onSpr = SPR_ENERGY_ON;
                break;
            case active_mode.item:
                switch (sections[idx].item)
                {
                    case item_category.pause:
                        SetSprOnSection(idx, SPR_ITEM_PAUSE,SPR_ITEM_PAUSE_ON);
                        break;
                    case item_category.shield:
                        SetSprOnSection(idx, SPR_ITEM_SHIELD, SPR_ITEM_SHIELD_ON);
                        break;
                    case item_category.speed:
                        SetSprOnSection(idx, SPR_ITEM_SPEED, SPR_ITEM_SPEED_ON);
                        break;
                    case item_category.heal:
                        SetSprOnSection(idx, SPR_ITEM_PAUSE, SPR_ITEM_PAUSE_ON);
                        break;
                }
                break;
            case active_mode.special:
                return; // special은 별도 함수를 호출해주자
        }
        sections[idx].actRend.sprite = sections[idx].normalSpr;
    }
    
    public void SetSprOnSection(int idx, Sprite norm, Sprite on,bool change = true)
    {
        sections[idx].normalSpr = norm;
        sections[idx].onSpr = on;
        if(change)sections[idx].actRend.sprite = norm;
        if (idx == onIdx) EnterSection(idx);
    }

    public int GetSleepIdx(bool use = true)
    {
        if (sleepSections.Count <= 0) return -1;
        int idx = UnityEngine.Random.Range(0,sleepSections.Count);
        if (use) sleepSections.RemoveAt(idx);
        return sleepSections[idx];
    }

    /// <summary>
    /// idx에 있는 section을 활성화시킨다 -> keepTime유지하면 akEvent가, remain뒤에는 arEvent가 일어난다. uEvent는 frame마다 update되면서 일어남
    /// </summary>
    bool ActiveSection(int idx, float keeptime, float remaintime, active_mode mode, IEnumerator<float> akEvent, IEnumerator<float> arEvent,IEnumerator<float> uEvnet = null) 
    { // complete :: keeptime등도 자유롭게 받을수 있게 + 타이머화
        if (sections.Length <= idx) return false;
        if (sections[idx].state == section_state.active) return false;

        sections[idx].actSign.SetActive(true);
        sections[idx].state = section_state.active;
        ChangeActiveMode(idx, keeptime, remaintime, mode, akEvent, arEvent, uEvnet);
        return true;
    }

    ///<summary>모드 체인지 :: energy, special, item, sleep으로 나뉜다</summary>
    bool ChangeActiveMode(int idx, float keeptime, float remaintime, active_mode mode, IEnumerator<float> akEvent, IEnumerator<float> arEvent, IEnumerator<float> uEvnet = null)
    {
        if (sections.Length <= idx) return false;
        if (sections[idx].state != section_state.active) return false;
        if (uEvnet != null)
            sections[idx].updateEvent = Timing.RunCoroutine(uEvnet);

        sections[idx].keepEnd = false;
        sections[idx].remainEnd = false;

        sections[idx].afterRemainEvent = arEvent;
        sections[idx].afterKeepEvent = akEvent;

        sleepSections.Remove(idx);

        sections[idx].keepTimer.SetTimer(keeptime);
        sections[idx].remainTimer.SetTimer(remaintime);

        sections[idx].mode = mode;
        UpdateActiveSpr(idx);
        return true;
    }


    //플레이어가 올라가있는 섹션 반환
    public int OnSection()
    {
        return onIdx;
    }

    public bool IsOnEnergy()
    {
        return (  sections[onIdx].state == section_state.active&&sections[onIdx].mode == active_mode.enegy );
    }

    public int SectionInTheta(float the)
    {
        for(int i=0; i<SECTION_NUM; i++)
            if (the >= SECTION_DEG * i && the < SECTION_DEG * (i + 1))
                return i;
        
        return 0;
    }
    
    public void ActiveHealingAtHome()
    {
        if (!healTimer.IsStop())
            return;
        
        if(ObjectManagerScr.Instance.GetPsc().GetHeal())
        {
            healTimer.resetTimer();
            homeRend.sprite = homeDisactive;
        }
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            enterDirection = ObjectManagerScr.Instance.GetPsc().direction; 
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            
            if (enterDirection == ObjectManagerScr.Instance.GetPsc().direction) // quest::한바퀴 돌았을때 처리
            {
                //ObjectManagerScr.Instance.GetPsc().ChangeLevel(enterDirection); // 한바퀴돌면 레벨업
            }
            
        }
    }

    public IEnumerator<float> SleepSection(int idx)
    {
        if (sections[idx].updateEvent != null)
            Timing.KillCoroutines(sections[idx].updateEvent);
        sections[idx].keepTimer.StopTimer();
        sections[idx].remainTimer.StopTimer();
        sections[idx].state = section_state.sleep;
        sections[idx].actSign.SetActive(false);

        sections[idx].afterKeepEvent = null;
        sections[idx].afterRemainEvent = null;
        sleepSections.Add(idx);
        yield break;
    }

    IEnumerator<float> WaitToEnergy(int idx) 
    {
        ChangeActiveMode(idx, ACTIVE_KEEPTIME, 0, active_mode.enegy, AfterKeepEnergy(idx), null);
        if (idx == onIdx) EnterSection(idx);
        yield break;
    }

    IEnumerator<float> AfterKeepEnergy(int idx)
    {
        SoundManager.PlaySFX(energySFX);
        OM.GetPsc().GetEnergy();
        waitCallList.Add(idx);
        Timing.RunCoroutine(SleepSection(idx));
        yield break;
    }

    IEnumerator<float> SleepItem(int idx)
    {
        Debug.Log("sleepItem");
        numItem--;
        Timing.RunCoroutine(SleepSection(idx));
        yield break;
    }

    /// <summary>
    /// itemFunc의 동작을 수행하고 Sleep시킴
    /// </summary>
    IEnumerator<float> RunItem(int idx,IEnumerator<float> itemFunc = null)
    {
        OM.GetPsc().GetItem(sections[idx].item);
        if (itemFunc!=null)Timing.RunCoroutine(itemFunc);
        Timing.RunCoroutine(SleepItem(idx));
        yield break;
    }
    
    /*
    IEnumerator<float> RunItemPause()
    {
        Boss boss = OM.GetBsc();
        boss.GetItemPause();
        yield break;
    }
    */
    
    /// <summary>
    /// 특정 각도에서, 원점에서의 거리
    /// </summary>
    public Vector2 WhereInDegreeFromStart(float degree, float distfromstart)
    {
        return WhereInDegree(degree, Radius - distfromstart);
    }

    public Vector2 WhereInDegree(float degree, float distfromdege = 0f)
    {
        degree -= 90; //밑에 계산식이 좌표축이 좀 다름
        if (degree > 360.0f) degree = degree - 360.0f;
        if (degree < 0.0f) degree = 360.0f + degree;

        float currentRadian = (degree) * DtoR;
        float x = (Radius - distfromdege) * Mathf.Cos(currentRadian);
        float y = (Radius - distfromdege) * Mathf.Sin(currentRadian);
        return new Vector2(x, y);
    }

    public float WhereTheta(float x, float y) // xy받아서 theta반환
    {
        if (x == 0 && y == 0) return 0;
        float d = Mathf.Sqrt(x * x + y * y);
        int dir = 1;
        if (y < 0) dir = -1;
        float degree = dir * Mathf.Acos(x / d) * RtoD + 90;
        if (degree >= 360.0f) degree = degree - 360.0f;
        if (degree < 0.0f) degree = 360.0f + degree;
        return degree;

    }



}
