﻿using System;
using System.Collections.Generic;
using DG.Tweening;
public class Timer // quest:: Timer도 gc안쓰게 바꾸자
{
    float setTime;
    float time; 
    bool isStop;
    bool isPause;
    
    public Timer()
    {
        isStop = true;
    }

    /// <summary>
    /// 5초면 t=5.0f
    /// </summary>
    public Timer(float t,bool stop = false)
    {
        
        SetTimer(t,stop);
    }

    public void SetTimer(float t,bool stop = false) 
    {
        setTime = time = t;
        isStop = stop;
        isPause = false;
    }

    public void Update(float dt)
    {
        if (isStop || isPause) return;
        time -= dt;

        if (time <= 0)
        {
            time = 0;
            isStop = true;
        }
    }

    public void SetPause(bool pause)
    {
        isPause = pause;
    }

    public void StopTimer()
    {
        time = 0;
        isStop = true;
    }

    public void resetTimer(bool stop=false)
    {
        SetTimer(setTime,stop);
    }
    
    /// <summary>
    /// SetTime을 반환(초기설정된 시간)
    /// </summary>
    public float GetSettime()
    {
        return setTime;
    }

    public float GetTime() // 저장된 시간(초)을 그대로 출력
    {
        return time;
    }
    
    public int GetMinute() // 저장된 시간을 분으로 표현
    {
        return (int)(time / 60);
    } 

    public int GetModSec() // 분과 초로 나눴을때 초 표현
    {
        return ((int)(time % 60));
    }
    
    public bool IsStop()
    {
        return isStop;
    }



}

