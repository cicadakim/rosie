﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MEC;

public class GameManagerScr : MonoBehaviour {
    
    public static GameManagerScr Instance = null;
    SceneManagerScr SCM;
    NumberManagerScr NM;
    SectionManagerScr SM;
    ObjectManagerScr OM;
    
    public Canvas canvas;

    public Text hintText;

    public bool eventOccur;

    public float boundTime;

    bool isTimePause = false;
    bool isPause = false;
    bool isEnd = false;

    public SpriteRenderer bgSR;
    public SpriteRenderer planetSR;
    public SpriteRenderer bhpSR;
    public SpriteRenderer bhp_ruler;
    
    public EndPanelScr endPanel;

    //Tutorial 관리 
    public GameObject tutorialManager;
    TutorialManagerScr TM;
    public Text systemText;

    Sequence TimeblankTween;

    bool isStarting = false;

    GameObject sceneManagerForTest;
    
    public AudioClip ingameBGM;
    public AudioClip countSFX;
    public AudioClip startSFX;
    public AudioClip hurrySFX;
    private float originTS;

    public GameObject pausePanel;

    //GameScene 모든 Manager 총괄
    //Awake에서 모든 Manager Singleton instance 만들어지면 GameManager Start에서 초기화시켜줌
    void Awake () {

        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    /// <summary>
    /// SceneManager에서 받아온 정보(이번 스테이지에 대한 정보) 적용
    /// </summary>
    void AdaptingameInfo()
    {
        if (SceneManagerScr.instance == null)
        {
            sceneManagerForTest = new GameObject();
            sceneManagerForTest.AddComponent<SceneManagerScr>();
            SCM = SceneManagerScr.instance;
        }
        else
        {
            SCM = SceneManagerScr.instance;
            bgSR.sprite = SCM.stageInfo.bg;
            planetSR.sprite = SCM.stageInfo.planet;
            bhpSR.sprite = SCM.stageInfo.bhp;
            bhp_ruler.sprite = SCM.stageInfo.bhp_ruler;
        }
    }

    void Start()
    {
        /// manager instance
        NM = NumberManagerScr.Instance; // NM은 딱히 init할게 없네
        OM = ObjectManagerScr.Instance;
        SM = SectionManagerScr.Instance;

        // 스테이지 정보에 맞게 초기화
        AdaptingameInfo();
        // 화면 해상도에 맞게 배경만 늘려줌. 게임 요소들은 16:9 유지(높이기반)
        bgSR.transform.localScale = new Vector3(SCM.bgScaleX, 1, 1);
        
        //tutorial 진행
        if (SCM!=null && SCM.stageInfo.isTutorial)
        {
            Instantiate(tutorialManager,Camera.main.transform);

            TM = TutorialManagerScr.Instance;
            TM.transform.localPosition = new Vector3(0, 0, 10);
            TM.StartTutorial(systemText);
            return;
        }
        else
        {
            SM.InitEnergyRandomly();
        }
        //실제 게임 진행
        StartEvent(EventGameStart());


    }

    private void OnApplicationPause(bool pause)
    {
        if(!isEnd && pause)
        {
            Pause(true);
        }
    }

    void Update () {
        if (eventOccur)
            return;

        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Pause(true);
            }
        }

        CheckTime();
    }

    /// <summary>
    /// puase 말고 일시정지 되는 이벤트 시작
    /// 이벤트 내부의 동작을 coroutine으로 구현해서 전달
    /// Pause는 timescale이 0이되고 event는 직접 update를 멈추는식으로
    /// MEC를 사용해서 나머지 코루틴을 짜고 기본 코루틴으로 Event단 구현
    /// </summary>
    public Coroutine StartEvent(IEnumerator cor)
    {
        return StartCoroutine(EventWait(cor));
    }

    IEnumerator EventWait(IEnumerator cor)
    {
        eventOccur = true;
        Timing.PauseCoroutines();
        OM.OnEventOccur();
        DOTween.PauseAll();
        yield return StartCoroutine(cor);
        OM.OnEventResume();
        DOTween.PlayAll();
        Timing.ResumeCoroutines();
        eventOccur = false;
        yield return Timing.WaitForOneFrame;
    }

    /// <summary>
    /// 게임스타트. 보스 초기화등
    /// </summary>
    /// <returns></returns>
    IEnumerator EventGameStart()
    {
        isStarting = true;
        if (SceneManagerScr.instance != null)
            OM.GenerateBoss(SCM.stageInfo.currentBoss,SCM.stageInfo.level);

        yield return StartCoroutine(OM.GetBsc().EventGrow()); //그로우 애니메이션
        OM.GetBsc().HPReset(3);
        SM.StartGenerateItem(3);

        System.GC.Collect(); // 시작하기전 garbage 제거
        yield return StartCoroutine(EventCountdown(3));

        SoundManager.SetCrossDuration(0);
        SoundManager.SetVolumeMusic(SoundManager.GetVolumeMusic() * 0.6f);
        //SoundManager.SetVolumeMusic(SoundManager.GetVolumeMusic()*1.5f);
        SoundManager.Play(ingameBGM, true);

        OM.StartWorking();
        isStarting = false;
    }

    public IEnumerator EventCountdown(float time)
    {
        float currentTime = time;

        NumberScr startNum = NM.CreateNumber((int)time,canvas.transform.position.x, canvas.transform.position.y, 0);
        //hintText.text = ObjectManagerScr.Instance.GetBsc().hintStr;
        while (currentTime > 0)
        {
            SoundManager.PlaySFX(countSFX);
            yield return new WaitForSeconds(1.0f);
            currentTime--;

            int showTime = (int)currentTime;
            if (showTime != 0)
                startNum.ChangeNum(showTime);
        }
        SoundManager.PlaySFX(startSFX);
        startNum.ReleaseNumber();
        hintText.enabled = false;
    }

    public void SetTimePause(bool pause = true)
    {
        isTimePause = pause;
    }


    void CheckTime()
    {
        if (isTimePause || isEnd) return;
        
        if (!isEnd)
        {
            ObjectManagerScr.Instance.GetPsc().Damaged(0.5f*Time.deltaTime);
            return;
        }
    }
    
    IEnumerator<float> CameraZoomInOut(float time)
    {
        Camera mainCamera = Camera.main;
        float originSize = mainCamera.orthographicSize;

        mainCamera.orthographicSize = originSize * 0.95f;

        float part = originSize * 0.05f / 10;

        while (mainCamera.orthographicSize <= originSize)
        {
            mainCamera.orthographicSize += part;
            yield return Timing.WaitForSeconds(time / 10);
        }
        mainCamera.orthographicSize = originSize * 1.02f;
        yield return Timing.WaitForSeconds(time/10);
        mainCamera.orthographicSize = originSize;
        yield break;
    }
    
    public void Pause(bool setPause)
    {
        if (!setPause)
        {
            if (!isPause) // 이미 resume
                return;
            isPause = !isPause;
            pausePanel.SetActive(false);
            if (SCM.stageInfo.isTutorial) // quest :: 옵저버로 하던...어케 해야할듯..
                TM.OnPause(false);

            Time.timeScale = originTS;
            if(!eventOccur)
                StartEvent(EventCountdown(3));
        }
        else
        {
            if (isPause) // 이미 pause
                return;
            isPause = !isPause;
            pausePanel.SetActive(true);
            if (SCM.stageInfo.isTutorial)
                TM.OnPause(true);
            originTS = Time.timeScale; // quest 코루틴화?
            Time.timeScale = 0;
        }
            

    }

    void OnEnd()
    {
        Time.timeScale = 1;
        SoundManager.StopMusic();
    }

    public void Restart()
    {
        OnEnd();
        SceneManagerScr.instance.GoToGame();
    }

    public void GetTouch()
    {
        if ( SCM!=null && SCM.stageInfo.isTutorial)
            TM.GetTouch();
        else
        {
            if(!eventOccur)
                OM.GetPsc().ChangeDirection();
        }
    }

    public void GoToMain()
    {
        OnEnd();
        Timing.RunCoroutine(GoMain());
    }
    
    IEnumerator<float> GoMain()
    {
        /*
        string strUrl = "http://www.naver.com";

        WebViewObject webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
        webViewObject.Init((msg) => {
            Debug.Log(string.Format("CallFromJS[{0}]", msg));
        });

        webViewObject.LoadURL(strUrl);
        webViewObject.SetVisibility(true);
        */
        Time.timeScale = 1;
        yield return Timing.WaitForOneFrame;
        SceneManagerScr.instance.GoToMain();
    }

    public void GoEnd(bool isWin)
    {
        if(SCM.stageInfo.isTutorial)
        {
            TM.GoEnd(isWin);
            return;
        }
        StartCoroutine(EventGoEnd(isWin));
    }

    /// <summary>
    /// 엔딩까지 가는걸 기달
    /// </summary>
    IEnumerator EventGoEnd(bool isWin)
    {
        isEnd = true;

        //Timing.RunCoroutine(CameraZoomInOut(0.3f));
        if (isWin)
        {
            StartCoroutine(OM.GetBsc().EventLose());
            StartCoroutine(OM.GetPsc().EventWin());
            yield return new WaitForSeconds(5.0f);
        }
        else
        {
            StartCoroutine(OM.GetBsc().EventWin());
            yield return StartCoroutine(OM.GetPsc().EventLose());
            yield return new WaitForSeconds(2.0f);
        }

        endPanel.PanelOn(isWin);
        while (true)
            yield return null; // event will not end.. 
    }

}
