﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletPool : ScrPool<BulletScr>
{
    Shooter shooter;
    public BulletPool(GameObject obj, Transform parent,Shooter s, int n = 0) : base(obj,parent,n)
    {
    }


    protected override void OnAdd(GameObject go)
    {
        BulletScr bs = go.GetComponent<BulletScr>();
        if (bs)
        {
            bs.InitProperty();
            bs.SetShooter(shooter);
        }
    }
}
