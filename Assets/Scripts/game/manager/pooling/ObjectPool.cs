﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool // quest:: 그냥 ScrPool로 통합시켜도 될거같긴함.. Transform으로 놓고 gameObject활용하게 하면 되니까
{
    protected List<GameObject> pool;
    protected List<int> sleepList;

    protected GameObject target;
    protected Transform targetParent;
    
    protected ObjectPool() // 상속에서 Init을 처리해주기위해
    {}

    public ObjectPool(GameObject obj,Transform parent, int n=0)
    {
        InitPool(obj,parent,n);
    }

    protected void InitPool(GameObject obj, Transform parent, int n)
    {
        target = obj;
        targetParent = parent;
        pool = new List<GameObject>(n);
        sleepList = new List<int>();

        for (int i = 0; i < n; i++)
            AddObject();
    }

    protected GameObject AwakeObject(bool isActive=true)
    {
        if (sleepList.Count <= 0) // pool의 크기를 늘려야함
            AddObject();
        int s = sleepList[sleepList.Count - 1];
        if (s >= pool.Count) return null;
        GameObject go = pool[s];
        go.SetActive(isActive);
        sleepList.RemoveAt(sleepList.Count - 1);
        return go;
    }

    public GameObject AwakeObjectAt(Vector2 pos, float theta=0) //화면의 어느 지점에서 Object를 Wake시킴
    {
        GameObject go = AwakeObject(false);
        go.transform.eulerAngles = new Vector3(0, 0, theta);
        go.transform.position = pos;

        go.SetActive(true);
        return go;
    }

    public bool SleepObject(GameObject go)
    {
        int idx = pool.FindIndex( (GameObject o) => o==go  );
        if (idx < 0) return false;
        pool[idx].SetActive(false);

        go.transform.position = Vector3.zero;
        //go.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        //quest :: Sleep시 추가적인 처리(Animator issue?)

        sleepList.Add(idx);
        return true;
    }

    protected GameObject AddObject() // pool에 객체화된 오브젝트 추가
    {
        GameObject go = MonoBehaviour.Instantiate(target,targetParent);
        
        go.SetActive(false);
        pool.Add(go);
        sleepList.Add(pool.Count-1);
        OnAdd(go);
        return go;
    }

    protected virtual void OnAdd(GameObject go)
    { 
    }
    
    public int GetSize()
    {
        return pool.Count - sleepList.Count;
    }
    
}
