﻿using UnityEngine;

public class RenderPool : ObjectPool // 한 object에서 그림만 다르게 사용될때
{
    /// <summary>
    /// BoxCollider2D 오브젝트만 일단..
    /// quest:: 다른 collider가 필요할까?
    /// </summary>
    /// 

    protected RenderPool() : base() { }

    public RenderPool(GameObject obj, Transform parent, int n=0):base(obj,parent,n)
    {
    }
    public GameObject WakeObjectAt(Vector2 pos, Sprite targetSpr, float theta=0)
    {
        GameObject go = base.AwakeObjectAt(pos, theta);
        if (targetSpr == null) return go;
        go.GetComponent<SpriteRenderer>().sprite = targetSpr;

        Vector3 sprb = targetSpr.bounds.size;
        BoxCollider2D c2d = go.GetComponent<BoxCollider2D>();
        c2d.size = sprb;
        return go;

        // bullet scale과 bound에관한 문제가 생길 수 있음.
    }
}
