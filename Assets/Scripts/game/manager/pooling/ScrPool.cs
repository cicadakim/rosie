﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// component T를 포함하는 gameObject target에 대한 pooling
/// Active를 수동으로 true,false해줘야함
/// </summary>
public class ScrPool<T> where T:Component
{
    protected List<T> pool;
    protected List<int> sleepList;

    protected GameObject target;
    protected Transform targetParent;

    protected ScrPool(){}
    public ScrPool(GameObject obj, Transform parent, int n = 0)
    {
        InitPool(obj, parent, n);
    }

    protected void InitPool(GameObject obj, Transform parent, int n)
    {
        target = obj;
        targetParent = parent;
        pool = new List<T>(n);
        sleepList = new List<int>();

        for (int i = 0; i < n; i++)
            AddObject();
    }

    void AddObject() // pool에 객체화된 오브젝트 추가
    {
        GameObject go = MonoBehaviour.Instantiate(target, targetParent);

        go.SetActive(false);
        T s = go.GetComponent<T>();
        pool.Add(s);
        sleepList.Add(pool.Count - 1);
        OnAdd(go);
    }
    
    public T AwakeObject(bool isActive = true)
    {
        if (sleepList.Count <= 0) 
            AddObject();
        int s = sleepList[sleepList.Count - 1];
        if (s >= pool.Count) return default(T);
        T scr = pool[s];
        
        if(isActive)
            scr.gameObject.SetActive(true);
        sleepList.RemoveAt(sleepList.Count - 1);

        return scr;
    }

    /// <summary>
    /// 회전을 local로 시킴 => parent의 기본 rotation값에 더해서 회전
    /// </summary>
    public T AwakeObjectAt(Vector2 pos, float theta = 0) //화면의 어느 지점에서 Object를 Wake시킴
    {
        T scr = AwakeObject(false);
        scr.transform.localEulerAngles = new Vector3(0, 0, theta);

        scr.transform.position = pos;

        scr.gameObject.SetActive(true);
        return scr;
    }

    protected virtual void OnAdd(GameObject go) {  }
    
    public bool SleepObject(T s)
    {
        int idx = pool.FindIndex((T se) => se.Equals(s));
        if (idx < 0) return false;
        s.gameObject.SetActive(false);
        //reuse를 위한 초기화. 
        s.gameObject.transform.localEulerAngles = Vector3.zero;
        s.gameObject.transform.localScale = Vector3.one;
        s.gameObject.transform.position = Vector3.zero;
        sleepList.Add(idx);
        return true;
    }

    public int GetSize()
    {
        return pool.Count - sleepList.Count;
    }

}

