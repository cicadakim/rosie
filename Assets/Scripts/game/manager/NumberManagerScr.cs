﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



///<summary>
/// 게임 내의 모든 Number Update를 진행한다
/// quest::NumberManager를 ActionManager처럼 조립패턴으로 해도 ㄱㅊ할듯 지금 Number Action관리하는부분이 너무 허술하다
///</summary>
public class NumberManagerScr : MonoBehaviour{

    public Canvas canvas;
    public Transform numGen;

    public Sprite[] SPR_NORM;
    public static NumberManagerScr Instance = null;

    public GameObject numberPref;
    public GameObject digitPref;

    ScrPool<DigitScr> digitPool;
    ScrPool<NumberScr> numberPool;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        digitPool = new ScrPool<DigitScr>(digitPref, numGen, 10);
        numberPool = new ScrPool<NumberScr>(numberPref, numGen , 10);
        
    }

    /// <summary>
    /// Parent transform must be in Canvas
    /// </summary>
    public NumberScr CreateNumber(int num,float x, float y,float theta,Sprite[] spriteset=null,Transform parent=null)
    {
        if (spriteset == null)
            spriteset = SPR_NORM;
        
        NumberScr ns = numberPool.AwakeObjectAt(new Vector2(x,y),0);
        ns.InitNum(num,spriteset,parent);
        return ns;
    }
    
    public void SleepNum(NumberScr num)
    {
        num.transform.SetParent(numGen);
        numberPool.SleepObject(num);
    }

    public DigitScr GetEmptyDigit()
    {
        return digitPool.AwakeObject();
    }

    public void SleepDigit(DigitScr digit)
    {
        digit.transform.SetParent(numGen);
        digitPool.SleepObject(digit);
    }


    /// <summary>
    /// return :: num의 자릿수 
    /// </summary>
    public int NumDigit(int num)
    {
        string s = num.ToString();
        return s.Length;
    }

    /// <summary>
    /// num에서 밑에서부터 i번째 자리 digit (i는 0부터)
    /// </summary>
    public int FindDigit(int idx, int num)
    {
        idx = idx + 1; // 0부터 입력값이 들어옴
        string s = num.ToString();
        int arrayIdx = s.Length - idx;
        return int.Parse(s[arrayIdx].ToString());
    }

}
