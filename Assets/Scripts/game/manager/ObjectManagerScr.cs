﻿using UnityEngine;
using System.Collections;

public class ObjectManagerScr : MonoBehaviour {

    public static ObjectManagerScr Instance = null;
    public GameObject player;
    PlayerScr ps;
    Boss bs;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        InitManager();
    }
    

    void InitManager()
    {
        ps = player.GetComponent<PlayerScr>();
    }
    
    public Boss GenerateBoss(GameObject bossPref,int level = 0)
    {
        //GameObject bss = GameObject.FindGameObjectWithTag("Boss");
       // if (bss != null)
        //    bss.SetActive(false);

        if (bossPref == null)
            return null; 
        GameObject go = Instantiate(bossPref, transform);
        Boss boss = go.GetComponent<Boss>();
        boss.SetLevel(level);
        return boss;
    }

    public void SetBoss(Boss boss)
    {
        bs = boss;
    }

    /// <summary>
    /// Awkae,Start로 초기화 후 보스와 플레이어를 움직이기 시작한다
    /// </summary>
    public void StartWorking()
    {
        GetBsc().OnGameStart(); 
        GetPsc().OnGameStart();
    }

    // event일어날때 object객체들에서 해야하는일하도록
    public void OnEventOccur()
    {
        if(ps!=null)ps.OnEventOccur();
        if(bs!=null)bs.OnEventOccur();
    }

    public void OnEventResume()
    {
        if (ps != null) ps.OnEventResume();
        if (bs != null) bs.OnEventResume();
    }
    
    public PlayerScr GetPsc() { return ps; }
    public Boss GetBsc() { return bs; }


    /*
public void ChangeBoss()
    {
        if(boss1.activeSelf)
        {
            boss1.SetActive(false);
            boss2.SetActive(true);
        }
        else
        {
            boss1.SetActive(true);
            boss2.SetActive(false);
        }
    }
    */
}
