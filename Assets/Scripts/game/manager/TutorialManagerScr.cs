﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MEC;
using System;

public class TutorialManagerScr : MonoBehaviour
{
    public static TutorialManagerScr Instance = null;

    Boss0Scr tutoBoss;

    GameManagerScr GM;
    PlayerScr player;

    public AudioClip ingameBGM;

    bool waitTouch = false;// touch기다릴때만 Touch에 반응
    bool getTouch = false; // touch 입력받음

    Text tutorialText;

    bool isControl = false; // 유저에게 컨트롤 권한이 있는지

    public SpriteRenderer tutoPanel;
    public Sprite[] tutoInfos;
    public SpriteRenderer touchSpr;

    GameObject shows; // Canvas에서 ingame정보 보여주기 토글
    Animator bossGrowEndEffect;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        GM = GameManagerScr.Instance;
    }

    private void Start()
    {
        tutoPanel.transform.localScale = new Vector3(SceneManagerScr.instance.bgScaleX, 1, 1);
        tutoPanel.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (player.hp < 10) player.Recovery(10.0f-player.hp);
    }

    public void StartTutorial(Text text)
    {
        SoundManager.SetCrossDuration(0);
        SoundManager.SetVolumeMusic(SoundManager.GetVolumeMusic() * 0.6f);
        //SoundManager.SetVolumeMusic(SoundManager.GetVolumeMusic()*1.5f);
        SoundManager.Play(ingameBGM, true);

        shows = GameObject.Find("Shows");
        bossGrowEndEffect = GameObject.Find("effect_bossgrowend").GetComponent<Animator>();
        bossGrowEndEffect.gameObject.SetActive(false);

        shows.SetActive(false);
        tutorialText = text;
        tutorialText.gameObject.SetActive(true);

        GM.SetTimePause(true);
        GM.StartEvent(Tutorial());
    }

    IEnumerator Tutorial()
    {
        player = ObjectManagerScr.Instance.GetPsc();
        yield return Phase1();
    }

    // beforeWait이후 Touch들어올때까지 대기
    IEnumerator WaitTouch(float beforeWait = 0)
    {
        yield return new WaitForSeconds(beforeWait);
        touchSpr.gameObject.SetActive(true);
        Sequence blk =  DOTweenCustom.Blink(touchSpr, -1, 0.4f,0.8f);
        waitTouch = true;
        while (!UseTouch())
        {
            yield return null;
        }
        blk.Kill();
        touchSpr.gameObject.SetActive(false);
        waitTouch = false;
    }

    IEnumerator activeTutoPanel(int idx,float waittime=0f)
    {
        yield return new WaitForSeconds(waittime);
        tutoPanel.sprite = tutoInfos[idx];
        tutoPanel.gameObject.SetActive(true);
    }


    // 이동에대한 설명
    IEnumerator Phase1()
    {
        isControl = false;
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(activeTutoPanel(0));
        yield return StartCoroutine(WaitTouch(0.8f));
        yield return GM.StartEvent(Phase2());
    }
    
    // 보스등장 
    IEnumerator Phase2()
    {
        tutoPanel.gameObject.SetActive(false);
        yield return Camera.main.DOShakePosition(2.0f).WaitForCompletion();
        bossGrowEndEffect.gameObject.SetActive(true);
        shows.SetActive(true);
        tutoBoss = (Boss0Scr)ObjectManagerScr.Instance.GenerateBoss(SceneManagerScr.instance.stageInfo.currentBoss);
        ObjectManagerScr.Instance.GetBsc().HPReset(0);

        yield return StartCoroutine(ObjectManagerScr.Instance.GetBsc().EventGrow());

        SectionManagerScr.Instance.GenerateEnergy(0);
        tutoPanel.gameObject.SetActive(true);
        for (int i=1; i<=2; i++)
        {
            tutoPanel.sprite = tutoInfos[i];
            yield return StartCoroutine(WaitTouch(0.8f));
        }
        System.GC.Collect();
        tutorialText.text = "";
        isControl = true;
        tutoPanel.gameObject.SetActive(false);
        //ObjectManagerScr.Instance.GetBsc().HPReset(3);
        yield return StartCoroutine(GM.EventCountdown(3));
        
        isControl = true;
        GM.SetTimePause(false);
        ObjectManagerScr.Instance.StartWorking();

        StartCoroutine(Phase2_2());
        
    }

    IEnumerator Phase2_2()
    {
        while ( ObjectManagerScr.Instance.GetPsc().combo==0  )
            yield return null;
        yield return null;
        isControl = false;
        GM.StartEvent(Phase2_3());
    }

    IEnumerator Phase2_3()
    {
        tutoPanel.gameObject.SetActive(true);

        for(int i=0; i<2; i++)
        {
            int idx = SectionManagerScr.Instance.GetSleepIdx();
            SectionManagerScr.Instance.GenerateEnergy(idx);
        }
        tutoPanel.sprite = tutoInfos[3];
        yield return StartCoroutine(WaitTouch(0.8f));
        tutoPanel.gameObject.SetActive(false);
        isControl = true;
        StartCoroutine(Phase2_4());
    }
    
    IEnumerator Phase2_4()
    {
        while (ObjectManagerScr.Instance.GetPsc().combo <=1)
            yield return null;
        yield return null;
        isControl = false;
        GM.StartEvent(Phase2_5());
    }

    IEnumerator Phase2_5()
    {
        tutoPanel.gameObject.SetActive(true);
        tutoPanel.sprite = tutoInfos[4];
        yield return StartCoroutine(WaitTouch(0.8f));
        tutoPanel.sprite = tutoInfos[5];
        yield return StartCoroutine(WaitTouch(0.8f));
        isControl = true;
        tutoPanel.gameObject.SetActive(false);
        StartCoroutine(Phase3());
    }

    IEnumerator Phase3()
    {
        Timing.RunCoroutine(tutoBoss.CantEvade());

        float originHp = player.hp;

        while (originHp <= player.hp)
            yield return null;
        yield return new WaitForSeconds(0.5f);
        GM.StartEvent(Phase3_1());
    }

    IEnumerator Phase3_1()
    {
        tutoPanel.gameObject.SetActive(true);
        tutoPanel.sprite = tutoInfos[6];
        yield return StartCoroutine(WaitTouch(0.8f));
        tutoPanel.gameObject.SetActive(false);
        StartCoroutine(Phase3_2());
    }

    IEnumerator Phase3_2()
    {
        float originHp = player.hp;

        while (originHp >= player.hp)
        {
            originHp = player.hp;
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        GM.StartEvent(Phase3_3());
    }

    IEnumerator Phase3_3()
    {
        tutoPanel.gameObject.SetActive(true);
        tutoPanel.sprite = tutoInfos[7];
        yield return StartCoroutine(WaitTouch(0.8f));

        tutoPanel.gameObject.SetActive(false);
        yield return GM.StartEvent(GM.EventCountdown(3));
        tutoBoss.StartPattern();
    }
    
    //마무리
    IEnumerator End()
    {
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ObjectManagerScr.Instance.GetBsc().EventLose());
        yield return new WaitForSeconds(2.0f);
        //보스 죽는 애니메이션 넣어주고싶음
        tutoPanel.gameObject.SetActive(true);
        tutoPanel.sprite = tutoInfos[8];
        yield return StartCoroutine(WaitTouch(0.8f));
        SceneManagerScr.instance.GoToMain();

    }
    
    /// <summary>
    /// End event
    /// </summary>
    public void GoEnd(bool isWin)
    {
        if(!isWin)
        {
            player.Recovery(10.0f);
        }
        else
        {
            GM.StartEvent(End());
        }

    }

    /// <summary>
    /// Touch입력 들어와있으면 true return하고 getTouch false로 돌려놓음
    /// </summary>
    bool UseTouch()
    {
        if(getTouch)
        {
            getTouch = false;
            return true;
        }
        return false;
    }

    public void GetTouch()
    {
        if (waitTouch)
            getTouch = true;
        if (isControl)
            player.ChangeDirection();
        

    }

    public void OnPause(bool isPause)
    {
        if(isPause)
        {
            tutoPanel.sortingOrder = 9;
        }
        else
        {
            tutoPanel.sortingOrder = 11;
        }

    }
}

