﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MEC;

public class EffectScr : MonoBehaviour {

    // effect script.
    // not loop , 특정한 effect state -> end state
    // active false상태 -> play하면 true

    UnityAction onEnd = null; // OnEnd Callback
    Animator anim;
    public bool isRunning = false;
    
	// Use this for initialization
	void Awake() {
        anim = GetComponent<Animator>();
	}

    IEnumerator<float> runSingleEffect(string state) // state must go to end state
    {
        anim.Play(state);
        isRunning = true;
        while (!anim.GetCurrentAnimatorStateInfo(0).IsName("end"))
            yield return Timing.WaitForOneFrame;
        isRunning = false;
        gameObject.SetActive(false);
        
        if (onEnd != null)
            onEnd();

        yield break;
    }

    public void SetOnEnd(UnityAction end)
    {
        onEnd = end;
    }

    public void StartEffect(string state)
    {
        gameObject.SetActive(true);
        Timing.RunCoroutine(runSingleEffect(state));
    }
	
}
