﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using System;

/// <summary>
/// 간단한 DOTween 응용
/// </summary>
public static class DOTweenCustom
{
    /// <summary>
    /// gameObject를 count 만큼 깜빡, 한번깜빡(켜졋다꺼졋다) 할때 시간 time_once
    /// min~1만큼깜빡. min은 0~1의 값을 가짐
    /// smmoth하게 깜빡이는게 기본 -> 딱딱 끊어지길 원할경우 isInstant
    /// </summary>
    public static Sequence Blink(SpriteRenderer sr,int count,float time_once,float min=0,bool isInstant = true)
    {
        float waitTime = isInstant ? time_once / 2 : 0;
        float blinkTime = isInstant ? 0 : time_once / 2;

        Sequence bt= DOTween.Sequence(); 
        bt.Append( sr.DOFade(min,blinkTime) );
        bt.AppendInterval(waitTime);
        bt.Append(sr.DOFade(1, blinkTime));
        bt.AppendInterval(waitTime);

        bt.SetLoops(count);
        return bt;
    }

    /// <summary>
    /// Blink 이미지버전
    /// </summary>
    public static Sequence Blink(Image sr, int count, float time_once, float min = 0, bool isInstant = true)
    {
        float waitTime = isInstant ? time_once / 2 : 0;
        float blinkTime = isInstant ? 0 : time_once / 2;

        Sequence bt = DOTween.Sequence();
        bt.Append(sr.DOFade(min, blinkTime));
        bt.AppendInterval(waitTime);
        bt.Append(sr.DOFade(1, blinkTime));
        bt.AppendInterval(waitTime);

        bt.SetLoops(count);
        return bt;
    }

    /// <summary>
    /// obj를 좌우폭 wide너비로 count만큼 shake, 한번 흔들리는데 걸리는 시간 oncetime
    /// </summary>
    public static Sequence ShakeObject(Transform obj, float wide, int count, float oncetime, bool isReal = false)
    {
        float t = oncetime / 4;
        
        Sequence st = DOTween.Sequence();
        st.Append(DOTween.To(() => obj.localPosition, x => obj.localPosition = x, new Vector3(-wide, 0, 0), t).SetRelative());
        st.Append(DOTween.To(() => obj.localPosition, x => obj.localPosition = x, new Vector3(wide, 0, 0), t).SetRelative());
        st.Append(DOTween.To(() => obj.localPosition, x => obj.localPosition = x, new Vector3(wide, 0, 0), t).SetRelative());
        st.Append(DOTween.To(() => obj.localPosition, x => obj.localPosition = x, new Vector3(-wide, 0, 0), t).SetRelative());//restart시 원래대로 튕겨서 돌아가기때문에 돌아가는 애니메이션 넣어줌
        st.SetLoops(count);
        st.OnComplete(() => obj.localPosition = obj.localPosition-new Vector3(obj.localPosition.x,0,0));
        
        return st;
    }

    public static IEnumerator<float> shakeObject(Transform obj, float wide, int count, float oncetime, bool isReal = false)
    {
        Debug.Log(obj.localRotation);
        Debug.Log(obj.rotation);

        float t = oncetime / 3;

        while(count-- > 0)
        {
            obj.localPosition = obj.localPosition + new Vector3(-wide, 0);
            yield return Timing.WaitForSeconds(t);
            obj.localPosition = obj.localPosition + new Vector3(wide, 0);
            yield return Timing.WaitForSeconds(t);
            obj.localPosition = obj.localPosition + new Vector3(0, 0);
            yield return Timing.WaitForSeconds(t);
        }
        yield return Timing.WaitForOneFrame;
    }
    

    /// <summary>
    /// Timing.WaitUntilDone의 argument로 넣어줌
    /// </summary>
    public static CoroutineHandle WaitForTween(Tween so)
    {
        return Timing.RunCoroutine(WaitFor(so));
    }

    static IEnumerator<float> WaitFor(Tween so)
    {
        while(so.IsPlaying())
            yield return Timing.WaitForOneFrame;
    }

    /// <summary>
    /// DoText 끝나고 리셋자동화
    /// speed의 속도로 글자 방출후 wait만큼 기다림
    /// speed는 한 글자 출력되는데 걸리는시간
    /// </summary>
    public static Sequence DOText(Text text,string str,float speed,float wait=0,bool isReset = true)
    {
        text.text = "";
        Sequence s = DOTween.Sequence();
        float duration = speed * str.Length;
        Debug.Log(duration);
        Tweener textTween = text.DOText(str, duration).SetEase(Ease.Linear);
        if (isReset) s.OnComplete(() => text.text = "");
        
        s.Append(textTween);
        s.AppendInterval(wait);

        return s;
    }
}
