﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MEC;
public class TitleSceneScr : MonoBehaviour {
    public SpriteRenderer title;
    public SpriteRenderer bg;
    public SpriteRenderer touch;

    private void Awake()
    {
        Screen.fullScreen = true;
    }
    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        bg.transform.localScale = new Vector3(SceneManagerScr.instance.bgScaleX, 1, 1); // 배경만 화면에 맞게 늘리거나 줄여줌 

        title.DOFade(1, 3f).OnComplete( ()=> {
            touch.gameObject.SetActive(true);
            touch.DOFade(0.5f, 1).OnComplete(
                ()=> {
                    touch.DOFade(1, 1).OnComplete(() =>DOTweenCustom.Blink(touch, -1, 1.5f, 0.6f,false));
                    Timing.RunCoroutine(WaitStart());
                    }
                );
        });
        title.transform.DOMove(Vector3.zero,3f);
          
    }

    IEnumerator<float> WaitStart()
    {

        while(true)
        {
            if (CheckInput())
                break;
            yield return 0f;
        }
          
    }

    bool CheckInput()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                SceneManagerScr.instance.GoToMain();
                return true;
            }
                

        }
        if (Input.touchCount <= 0) return false;

        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Began)
            {
                SceneManagerScr.instance.GoToMain();
                return true;
            }
        }
        return false;
    }


}
