﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneScr : MonoBehaviour
{
    public Transform bg;
    public AudioClip bgm;
    public AudioClip buttonSFX;

    bool backPushed = false;

    public ToStageBtnScr tutorialBtn;
    Image tutorialBtnImg;

    private void Awake()
    {
        if (SceneManagerScr.instance == null)
        {
            GameObject sceneManagerForTest = new GameObject();
            sceneManagerForTest.AddComponent<SceneManagerScr>();
        }
        //Sound setting
        SoundManager.SetCrossDuration(0);
        tutorialBtnImg = tutorialBtn.GetComponent<Image>();
        //DOTweenCustom.Blink(tutorialBtnImg, -1, 0.3f, 0.8f);

    }
    private void Start()
    {
        //PlayerPrefs.DeleteAll(); // 테스트용



        int hasPlayed = PlayerPrefs.GetInt("HasPlayed");

        if (hasPlayed == 0)
        {
            // First Time
            // Go to tutorial
            PlayerPrefs.SetInt("HasPlayed", 1);
            tutorialBtn.GoToStage(true);
        }
        else
        {
            // Not First Time
            SoundManager.Play(bgm, true);
            bg.localScale = new Vector3(SceneManagerScr.instance.bgScaleX, 1, 1); // 배경만 화면에 맞게 늘리거나 줄여줌   
        }
    }

    private void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                if(backPushed)
                {
                    Application.Quit();
                }
                else
                {
                    StartCoroutine(WaitPushBack());
                    backPushed = true;
                }
            }
        }
    }

    IEnumerator WaitPushBack()
    {
        yield return new WaitForSeconds(3);
        backPushed = false;
    }

    public void GoToIntro()
    {
        SoundManager.PlaySFX(buttonSFX);
        SceneManagerScr.instance.GoToIntro();
    }

    public void GoToTutorial()
    {
        SceneManagerScr.instance.GoToTutorial();
    }
    
}
