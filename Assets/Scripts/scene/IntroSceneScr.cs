﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MEC;
public class IntroSceneScr : MonoBehaviour {

    public SpriteRenderer planetSR;
    public SpriteRenderer bgSR;
    
    private void Start()
    {
        planetSR.sprite = SceneManagerScr.instance.stageInfo.planet;
        bgSR.sprite = SceneManagerScr.instance.stageInfo.bg ;
        bgSR.transform.localScale  = new Vector3(SceneManagerScr.instance.bgScaleX, 1, 1);
        Timing.RunCoroutine( RotateAndStartGame(180, 2) );
    }

    IEnumerator<float> RotateAndStartGame(float theta, float duration)
    {
        Tweener r = transform.DORotate( new Vector3(0,0,theta), duration).SetEase(Ease.InOutQuart);
        while( r.IsPlaying()  )
            yield return Timing.WaitForOneFrame;
        
        SceneManagerScr.instance.GoToGame();
    }


}
