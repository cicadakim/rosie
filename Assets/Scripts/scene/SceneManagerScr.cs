﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScr : MonoBehaviour {
    public struct gameToEnd
    {
        public bool win;
    }

    public struct StageInfo
    {
        public GameObject currentBoss;
        public Sprite bg;
        public Sprite planet;
        public Sprite bhp;
        public Sprite bhp_ruler;
        public bool isTutorial; // quest :: 지저분~
        public int level; // quest 난이도..지저분
    }

    public void SetStageInfo(Sprite bg, Sprite planet, GameObject bossPref, Sprite bhp_gauge, Sprite bhp_ruler)
    {
        stageInfo.currentBoss = bossPref;
        stageInfo.planet = planet;
        stageInfo.bg = bg;
        stageInfo.bhp = bhp_gauge;
        stageInfo.bhp_ruler = bhp_ruler;
    }

    public StageInfo stageInfo;
    public gameToEnd GtE;
    public static SceneManagerScr instance=null;

    const float fixRatio = 16.0f / 9.0f;
    float screenRatio;
    public float bgScaleX; // 레터박스를 대신하기위해서 배경화면만 화면에 맞게 늘리거나 줄여주기 위함

    public bool isSoundOn = true;
    public bool isVibeOn = true; // quest :: 얘네를 여기다 넣는게 맞나?

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;

        screenRatio = (float)Screen.width / (float)Screen.height;

        Screen.SetResolution((int)((float)Screen.height * fixRatio), Screen.height, true); // 게임플레이는 fixRatio에 맞게 늘리거나 줄이고
        bgScaleX = screenRatio / fixRatio; // 배경만 화면에 맞게 늘리거나 줄여줌

        SettingSoundVibe();
    }

    void SettingSoundVibe()
    {

        int soundOn = PlayerPrefs.GetInt("soundOn", 1);
        int vibeOn = PlayerPrefs.GetInt("vibeOn", 1);

        isSoundOn = soundOn == 1 ? true : false;
        isVibeOn = vibeOn == 1 ? true : false;

        if (isSoundOn)
            SoundManager.Mute(false);
        else
            SoundManager.Mute(true);
    }

    public void SetBoss(GameObject pref)
    {
        stageInfo.currentBoss = pref;
    }

    public void SetBG(Sprite bg)
    {
        stageInfo.bg = bg;
    }

    public void SetPlanet(Sprite planet)
    {
        stageInfo.planet = planet;
    }

    public void SetBHP(Sprite bhp)
    {
        stageInfo.bhp = bhp;
    }

    public void SetBRuler(Sprite b_ruler)
    {
        stageInfo.bhp_ruler = b_ruler;
    }

    public GameObject GetBoss()
    {
        return stageInfo.currentBoss;
    }

    public void GoToMain()
    {
        SceneManager.LoadScene("main");
    }

    public void GoToIntro()
    {
        stageInfo.isTutorial = false;
        SceneManager.LoadScene("intro");
    }

    public void GoToTutorial()
    {
        stageInfo.isTutorial = true;
        SceneManager.LoadScene("intro");
    }

    public void GoToGame()
    {
        SceneManager.LoadSceneAsync("game");
    }
    

    
}
