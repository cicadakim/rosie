﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneScr : MonoBehaviour {
    
    public void EnterGameScene()
    {
        SceneManager.LoadScene("intro");
    }
}
