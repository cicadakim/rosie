﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndingSceneScr : MonoBehaviour {

    public Image resultImage;
    public Sprite winSpr;
    public Sprite loseSpr;

	// Use this for initialization
	void Start () {
        if (SceneManagerScr.instance.GtE.win)
            resultImage.sprite = winSpr;
        else
            resultImage.sprite = loseSpr;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ReturnToGame()
    {
        SceneManagerScr.instance.GoToGame();
    }

    public void GoToMain()
    {
        SceneManagerScr.instance.GoToMain();
    }
}
