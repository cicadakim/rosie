﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BlockControllerScr : MonoBehaviour {

    public Transform[] blocks;
    Sequence[] blockTweens;

    public delegate Sequence setTween(Transform tf);

    private void Awake()
    {
        blockTweens = new Sequence[blocks.Length];
    }
    
    /// <summary>
    /// Block에 Action 적용 :: (Transform t)=>{  sequence = Sequence.Append(t.DoTween())  return sequence}
    /// </summary>
    public Sequence TweenBlockWith(int idx,setTween tweenWith)
    {
        if(idx>=0 && idx<blocks.Length)
        {
            //DOTween.Kill(blocks[idx]);
            blockTweens[idx] = tweenWith(blocks[idx]);
            return blockTweens[idx];
        }
        
        return null;
    }

    /// <summary>
    /// All이니까 적용된 Sequence중 그냥 아무거나 하나 리턴해줌
    /// </summary>
    public Sequence TweenAllBlocksWith(setTween tweenWith)
    {
        Sequence s = null;
        for (int i = 0; i < blocks.Length; i++)
            s = TweenBlockWith(i,tweenWith);
        return s;
    }

    public Sequence GetBlockTween(int idx)
    {
        if (idx >= 0 && idx < blockTweens.Length)
            return blockTweens[idx];
        return null;
    }

    /// <summary>
    /// Stop all Tween in blocks
    /// </summary>
    void StopTweenBlocks()
    {
        if (blockTweens == null)
            return;
        for (int i = 0; i < blockTweens.Length; i++)
            blockTweens[i].Kill();
    }

    public void PauseAll()
    {
        if (blockTweens == null)
            return;
        for (int i = 0; i < blockTweens.Length; i++)
            blockTweens[i].Pause();
    }

    public void ResumeAll()
    {
        if (blockTweens == null)
            return;
        for (int i = 0; i < blockTweens.Length; i++)
            blockTweens[i].Play();
    }

    public int GetBlockLength()
    {
        return blocks.Length;
    }
}
