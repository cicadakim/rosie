﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBlockScr : MonoBehaviour {
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            ObjectManagerScr.Instance.GetPsc().EnterAttack(20);
        }
    }
}
